#include <forward_list>
#include <iostream>

int main() {
    std::forward_list<int> list{1, 2, 3,};

    std::front_insert_iterator<std::forward_list<int>> fi_it = std::front_inserter(list);
    *fi_it = 0;  // add a value at the front

    // will not work as no member 'push_back' in std::forward_list
    std::back_insert_iterator<std::forward_list<int>> bi_it = std::back_inserter(list);
    // *bi_it = 4;  // add a value at the back


    for (const auto val : list) {
        std::cout << val << ' ';
    }
    

    return 0;
}
