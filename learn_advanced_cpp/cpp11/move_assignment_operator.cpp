#include <iostream>
#include <vector>

class Test {
public:
    Test() {
        pBuffer_ = new int[SIZE]{};                                         // zeroed array
    }
    Test(int val) {
        pBuffer_ = new int[SIZE]{};                                         // code repetition
                                                                            // default c'tor should be called
        for (int i = 0; i < SIZE; i++) {
            pBuffer_[i] = val;
        }
    }
    Test(const Test& other) : Test() {                                      // default c'tor call to init pBuffer
        std::cout << "copy constructor" << std::endl;
        // pBuffer_ = new int[SIZE]{};                                      // code repetition
                                                                            // default c'tor is called above
        memcpy(pBuffer_, other.pBuffer_, sizeof(*pBuffer_) * SIZE);         // dereference then multiply
    }
    /* move constructor */
    Test(Test&& other) {
        std::cout << "move constructor" << std::endl;
        pBuffer_ = other.pBuffer_;
        other.pBuffer_ = nullptr;
    }
    /* move assignment operator */
    Test& operator=(Test&& other) {
        std::cout << "move assignment operator" << std::endl;
        delete[] pBuffer_;
        pBuffer_ = other.pBuffer_;
        other.pBuffer_ = nullptr;

        return *this;
    }
    /* copy assignment operator */
    Test& operator=(const Test& other) {
        std::cout << "copy assignment operator" << std::endl;
        // pBuffer_ = new int[SIZE]{};                                      // code repetition
        // memcpy(pBuffer_, other.pBuffer_, sizeof(*pBuffer_) * SIZE);      // copy c'tor should be called instead
        Test temp(other);
        std::swap(temp, *this);

        return *this;
    }

    ~Test() {
        delete[] pBuffer_;
    }

    friend std::ostream& operator<<(std::ostream& out, const Test& test);

private:
    static const int SIZE{ 100 };
    int* pBuffer_{};                                                        // nullptr initialization
};

std::ostream& operator<<(std::ostream& out, const Test& test) {
    out << "Hello from test";
    return out;
}

Test getTest() {
    return Test();                                                          // returning by value
                                                                            // should return a copy
}

int main() {
    Test test;
    test = getTest();                                                       // move assignment invoked

    return 0;
}
