#include <iostream>
#include <string>

class BufferManager {
  public:
    BufferManager() {}
    // strong exception guarantee (no instances are created or modified in an error condition)
    BufferManager(int size) : size_(size) {
        buffer_ = new char[size];                   // only 'new' can throw an exception without allocating memory
    }
    // strong exception guarantee
    ~BufferManager() {
        delete [] buffer_; // defined as noexcept
    }

    // strong exception guarantee (copy constructor)
    BufferManager(const BufferManager& other) {
        // no need to check for self-assignment (not possible)
        size_ = other.size_;
        buffer_ = new char[size_];                // only 'new' can throw an exception without allocating memory
        for (size_t i = 0; i < size_; ++i) {
            buffer_[i] = other.buffer_[i];
        }
    }

    // BASIC EXCEPTION GUARANTEE (assignment operator)
    BufferManager& operator=(const BufferManager& rhs) {
        // checking for self-assignment (to avoid destroying old data before copying in the new one)
        if (this != &rhs) {
            size_ = rhs.size_;
            delete [] buffer_;                      // avoiding memory leak thus BASIC GUARANTEE
            buffer_ = new char[size_];              // only 'new' can throw an exception without allocating memory
                                                    // but POINTER is already INVALIDATED (memory released)
            for (size_t i = 0; i < size_; ++i) {    
                buffer_[i] = rhs.buffer_[i];
            }
        }

        return *this;
    }

  private:
    int size_{};
    char* buffer_{nullptr};
};

int main() {
    /* error: no way to assign a newly created object to itself in a way that calls to copy constructor
    BufferManager buffA(buffA); */

    BufferManager buffA{5};
    BufferManager buffB{buffA};
   
    BufferManager buffC;
    buffC = buffA;

    return 0;
}
