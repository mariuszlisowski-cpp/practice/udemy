#include <iostream>
#include <map>
#include <unordered_map>
#include <string>

template<typename Map>
void print_map(const Map& map) {
    for (const auto& pair : map) {
        std::cout << pair.first  << " : " << pair.second << std::endl;
    }
    std::cout << std::endl;
}

int main() {
    // hash map
    std::unordered_multimap<std::string, int> scores {
        {"tom", 1},
        {"ola", 2},
        {"ela", 3},
        {"ola", 4},
        {"ala", 5}
    };
    print_map(scores); // not sorged (organized into buckets)

    /* no reverse iterators - forward only */    
    for (auto it = scores.begin(); it != scores.end(); ++ it) {}

    /* no lower nor upper_bound - equal_range only */
    auto range_pair_it = scores.equal_range("ola");
    for (auto it = range_pair_it.first; it != range_pair_it.second; ++it) {
        std::cout << it->first << std::endl;
    }

    auto it = scores.find("tom");
    if (it != scores.end()) {
        std::cout << "> found: " << it->second << std::endl;
    }

    auto count = scores.count("ola");
    std::cout << "> count: " << count << std::endl;
    
    return 0;
}
