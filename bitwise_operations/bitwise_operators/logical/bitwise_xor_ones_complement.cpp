/* set all bits to 1 -  exclusive OR - symbol ^ */

#include <bitset>
#include <cstdint>
#include <iostream>

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1

   value  10001100
          XOR
   value  10001100
          ________
          00000000
   ~      11111111
    
   truth table:
   0 XOR 0 -> 0       both the same -> 0
   0 XOR 1 -> 1
   1 XOR 0 -> 1
   1 XOR 1 -> 0       both the same -> 0
*/

int main() {
    uint8_t value{0b10001100};
    std::cout << std::bitset<8>(value) << std::endl;

    auto mask(0b11111111);
    value  ^= mask;                                             // ones complement

    std::cout << std::bitset<8>(value) << std::endl;

    return 0;
}
