#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int function() {
    static int i{5};
    return i++;
}

class Clazz {
  public:
    int operator()() { return i--; }

  private:
    int i{4};
};

template <class T> void print_container(const std::vector<T>&);

int main() {
    constexpr size_t size = 5;
    std::vector<int> v; // empty

    std::generate_n(std::back_inserter(v), size,
                    [n = size]() mutable { return n--; }); // static not captured n
    print_container(v);

    return 0;
}

template <class T> void print_container(const std::vector<T>& cont) {
    std::for_each(cont.begin(), cont.end(),
                  [](const T& s) { std::cout << s << ' '; });
    std::cout << std::endl;
}
