#include <bitset>
#include <iostream>

int main() {
    int16_t positive{32767};                                            // int16 [-32768, 32767]
    int16_t negative = ~positive;                                       // 1's complement (invert)
    negative += 1;                                                      // 2's complement (plus one)
    
    std::cout << positive << std::endl;
    std::cout << negative << std::endl;
    std::cout << std::bitset<16>(positive) << std::endl;
    std::cout << std::bitset<16>(negative) << std::endl;
    
    return 0;    
}
