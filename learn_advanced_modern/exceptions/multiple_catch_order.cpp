#include <exception>    // exception
#include <iostream>
#include <stdexcept>    // out_of_range
#include <vector>

int main() {
    std::vector<int> vec(2);                                        // capacity

    try {
        auto value = vec.at(2);                                     // index too high
    } catch (const std::exception& e) {                             // dynamic binding as caught by reference
        std::cerr << "> exception: " << e.what() << std::endl;      // handles all subclasses of exception
    } catch (const std::out_of_range& e) {                          // handles only out_of_range (also suitable)
        std::cerr << "> out of range: " << e.what() << std::endl;   // could be caught if was earlier in code
    }

    // reversed order
    try {
        auto value = vec.at(2);
    } catch (const std::out_of_range& e) {                          // caught first
        std::cerr << "> out of range: " << e.what() << std::endl;
    } catch (const std::exception& e) {                             // not caught
        std::cerr << "> exception: " << e.what() << std::endl;
    }

    return 0;
}
