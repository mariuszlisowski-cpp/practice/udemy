#include <iostream>
#include <map>

int main() {
    std::multimap<int, std::string> lookup;
    lookup.insert(std::make_pair(30, "Mike"));
    lookup.insert(std::make_pair(10, "Elen"));
    lookup.insert(std::make_pair(30, "Mary"));                                                  // duplicate key
    lookup.insert(std::make_pair(20, "Gary"));

    for (auto& el : lookup) {
        std::cout << el.first << ": " << el.second << std::endl;
    }

    // find same keys
    std::pair<std::multimap<int, std::string>::iterator,
              std::multimap<int, std::string>::iterator>
        its = lookup.equal_range(30);                                                           // same keys
    
    /* or using c++11
    auto its = lookup.equal_range(30); */

    // loop over iterator range (first & second of a pair)
    for (std::multimap<int, std::string>::iterator it{ its.first }; it != its.second; ++it) {
        std::cout << "> same " << it->first << ": " << it->second << std::endl;
    }

    return 0;
}
