#include <iomanip>
#include <forward_list>
#include <iostream>
#include <iterator>
#include <string>

template<class T>
void print_container(const T& container, std::string message) {
    std::cout << std::setw(25) << std::left << message;
    for (auto el : container) { std::cout << el << ' '; }
    std::cout << std::endl;
}

int main() {
    std::forward_list<int> fl{2, 3, 4}; // singly linked list (constists of nodes with the next pointer)

    fl.push_front(1);
    print_container(fl, "> pushed front:");
    
    fl.pop_front();
    print_container(fl, "> popped front:");

    fl.reverse();
    print_container(fl, "> reversed:");

    fl.insert_after(fl.cbegin(), 9);
    fl.insert_after(std::next(fl.begin(), 3), 9); // iterator increment only (undefined behaviour if decremented)
    print_container(fl, "> inserted after:");

    fl.remove(9); // FAST, no need to erase after (all nodes with such a value deleted)
    print_container(fl, "> removed:");

    fl.emplace_front(3);
    print_container(fl, "> emplaced front:");

    fl.unique(); // must be adjacent to be removed
    print_container(fl, "> not made unique:");

    fl.sort(); // no random access to elements thus internal implementation (instead of std::sort())
    print_container(fl,  "> sorted:");

    fl.unique(); // adjacent after sorting thus removed
    print_container(fl, "> made unique:");

    return 0;
}
