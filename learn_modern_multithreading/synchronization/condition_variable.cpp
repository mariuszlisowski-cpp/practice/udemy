#include <condition_variable>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>

using namespace std::literals;

std::mutex the_mutex;
std::condition_variable cv;
std::string shared_data{"started"};

// waiting thread
void reader() {
    std::unique_lock<std::mutex> ul(the_mutex);
    std::cout << "> read thread waiting...\n";
    cv.wait(ul);                                        // wait for condition variable to be notified
    std::cout << "> read thread has "
              << shared_data << std::endl;              // critical section (executed after waking up)
}

// modifying thread
void writer() {
    std::cout << "> writing data..." << std::endl;
    {
        std::lock_guard<std::mutex> lg(the_mutex);
        std::this_thread::sleep_for(1s);
        shared_data = "finished";                       // critical section
    }
    cv.notify_one();                                    // notify condition variable to wake up other thread
}

int main() {
    std::thread read(reader);                           // waits until notified by write thread
    std::thread write(writer);

    read.join();
    write.join();

    return 0;
}
