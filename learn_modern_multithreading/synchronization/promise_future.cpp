#include <functional>
#include <future>
#include <iostream>
#include <thread>

using namespace std::literals;

void producer(std::promise<int>& p_value) {
    std::cout << "> setting a value..." << std::endl;
    std::this_thread::sleep_for(1s);
    p_value.set_value(9);                               // updating a shared value
}

void consumer(std::future<int>& f_value) {
    int x{f_value.get()};                               // wait until value is updated by promise
    std::cout << "> value consumed" << std::endl;
}

int main() {
     std::promise<int> promise;
     std::future<int> future{promise.get_future()};     // associated future

    std::thread consume(consumer, std::ref(future));
    std::thread produce(producer, std::ref(promise));

    consume.join();
    produce.join();

    return 0;
}
