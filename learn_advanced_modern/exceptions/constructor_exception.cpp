#include <ctime>
#include <iostream>
#include <stdexcept>

class Car {
public:
    Car(int year_of_manufacture) : year_of_manufacture_(year_of_manufacture) {
        if (year_of_manufacture > ltm->tm_year + 1900) {
            throw std::invalid_argument("The car was manufacuted in the past, wasn't it?");
        }
    }
    // exceptions must be handled inside destructor
    // DO NOT throw exceptions in destructor as an object may be destructed while stack is unwinding
    ~Car() {} // one stack unwinding can occur at a time thus throwing another exception results in undefined behaviour


private:
    std::time_t now = std::time(0);
    tm* ltm = std::localtime(&now);
    int year_of_manufacture_;
};

int main() {
    // constructor throwing an exception
    try {
        std::cout << "Selling a car..." << std::endl;
        Car isuzu_wizard(2029);                                 // object destroyed if exception is thrown
        std::cout << "A car has been bought." << std::endl;
    } catch (const std::invalid_argument& e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}
