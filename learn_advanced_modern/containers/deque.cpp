#include <deque>
#include <iostream>

int main() {
    // double-ended queue (indexed sequence container)
    std::deque<int> deq{2, 3, 4}; // stored in several memory blocks (unlike a vector which stores in one block)
    deq.push_front(1);
    deq.push_back(5);

    // iterators implemented(opposed to std::queue)
    for (auto el : deq) {
        std::cout << el << ' ';
    }

    return 0;
}
