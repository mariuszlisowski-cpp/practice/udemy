#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <vector>

template<typename T, typename K>
void print_map(std::multimap<T, K> map) {
	for (const auto& [key, value]: map) { 		// c++17
		std::cout << key << " : " << value << std::endl;
	}
}

template<typename X,
         typename Y,
         template<typename, typename> class Pair,
         template<typename...> class Vector>
void print_vector_of_pairs(Vector<Pair<X, Y>> pairs)
{
	for (Pair<X, Y> pair : pairs) {
		std::cout << pair.first << " : " << pair.second << std::endl;
	}

}

int main() {
	std::multimap<std::string, size_t> players {
		{"Tom", 2},	{"Ele", 4},	{"Tom", 3},	{"Kim", 6},	{"Tom", 4}
	};

	size_t total_score{};
	for (const auto& [name, score] : players ) {
		if (name == "Tom") {
			total_score += score;
			std::cout << name << " has " << score << " points" << std::endl;
		}
	}
	std::cout << "Tom's total score: " << total_score << std::endl;

    auto it = players.find("Tom");              // first occurence
    if (it != players.end()) {
        auto count = players.count("Tom");      // number of occurences
        while (count--) {
            std::cout << "> " << it->first << " found with socre " << it->second << std::endl;
            ++it;
        }
    }
    
	/* no subscripting possible
       size_t point = players["Tom"]; // error
	 */
	
	std::multimap<int, char> nums{
		{1, 'a'}, {2, 'b'}, {3, 'c'}, {3, 'd'}, {3, 'e'}, {3, 'e'}, {3, 'e'}, {4, 'f'}
	};
	print_map(nums)	;

	auto lower = nums.lower_bound(2); // greater than or equal to
	auto upper = nums.upper_bound(2); // greater than
	if (upper != nums.end() && lower != nums.end()) {
		std::cout << "> lower bound from: " << lower->second << std::endl;
		std::cout << "> upper bound from: " << upper->second << std::endl;
	}

	// range (bounds)
	lower = nums.lower_bound(3); // finds first 3
	upper = nums.upper_bound(3); // finds 4
	for (auto it = lower; it != upper; ++it) {
		std::cout << it->second << ' ';
	}
	std::cout << std::endl;

	// range (equal)
	auto range_pair = nums.equal_range(3); // second iterator excluded from range (alike .end())
	std::for_each(range_pair.first, range_pair.second,
	              [](std::pair<int, char> pair){ std::cout << pair.second << ' '; });
	std::cout << std::endl;

	// generic algorithms
	auto first_found_it = std::find_if(range_pair.first, range_pair.second,
	                                   [](const std::pair<int, char>& pair){
 	 			    			           return pair.second == 'e';
			    				       });
	if (first_found_it != range_pair.second) {
		std::cout << "> found first within range: " << first_found_it->second << std::endl;
	}

	std::cout << "> found all within range: ";
	while (first_found_it != range_pair.second) {
		std::cout << first_found_it->second << ", ";
		++first_found_it;
	}

	std::vector<std::pair<int, char>> results;
	std::copy_if(range_pair.first, range_pair.second,
	             std::back_inserter(results),
				 [](const std::pair<int, char>& pair){
					 return pair.second == 'e';
			     });
	std::cout << std::endl;
	print_vector_of_pairs(results);

	return 0;
}
