#include <fstream>
#include <iostream>
#include <string>

// accepts rvalues only (prvalues & xvalues)
std::string print(std::string&& str) {
    std::cout << "> printing: " << str << std::endl; // lvalue here

    return std::string(str); // never use std::move while returning (will be done automatically)
}

// actually accepts rvalue reference (as streams cannot be copied)
void print_file_value(std::fstream fs) {
    std::cout << "> ownership of a file transfered" << std::endl;
    fs.close(); // lvalue here
}

// accepts rvalue reference (directly)
void print_file_rvalue(std::fstream&& fs) {
    std::cout << "> printing file directly" << std::endl;
    fs.close(); // lvalue here
}

int main() {
    std::string strA{"persistent"};

    // casting to rvalue
    print(std::move(strA));                         // should be named rvalue_cast Stroustrup said

    /* streams cannot be copied
    print_file_value(file); // error*/

    std::fstream file("filename");
    print_file_value(std::move(file));              // casting to rvalue (instance disposed of)
    print_file_rvalue(std::fstream("filename"));    // already rvalue

    return 0;
}
