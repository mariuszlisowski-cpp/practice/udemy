#include <algorithm>
#include <execution>
#include <iostream>
#include <vector>

// using namespace std::execution;

int main() {
    std::vector<int> v{5, 6, 7, 3, 2};
    
    std::sort(v.begin(), v.end());
    
    /* Apple Clang does not yet have support
       for the parallel algorithms extensions
    std::sort(std::execution::seq, v.begin(), v.end());             // sequential
    std::sort(std::execution::par, v.begin(), v.end());             // parallel
    std::sort(std::execution::par_unseq, v.begin(), v.end());       // parallel and vectorized (unsequenced)
    std::sort(std::execution::unseq, v.begin(), v.end());           // vectorized (c++20)
    */
    return 0;
}
