#include "../inc/car.hpp" // CMake: no need for relative path as include directory known (IDE compliant)

#include <string>

Car::Car(const std::string& make, const std::string& model)
        : make_(make)
        , model_(model)
        , mileage_(0) {
            time_ = boost::posix_time::second_clock::local_time();
            year_ = time_.date().year();
        }

std::ostream& operator<<(std::ostream& os, const Car& car) {
    os << "A new car was bought:"
        << "\n> make:    " << car.make_
        << "\n> model:   " << car.model_
        << "\n> year:    " << car.year_ << std::endl;

    return os;
}
