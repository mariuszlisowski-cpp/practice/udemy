#include <chrono>
#include <iostream>
#include <string>
#include <thread>

using namespace std::literals;

int main() {
    std::cout << "Starting..." << std::endl;
    
    std::this_thread::sleep_for(2s);
    
    std::cout << "Finished!" << std::endl;

    return 0;
}
