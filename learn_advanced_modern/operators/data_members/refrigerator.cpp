#include "refrigerator.hpp"

Refrigerator::Refrigerator(double temp, bool door, bool power)
    : temperature_(temp), door_open_(door), power_on_(power) {}

std::ostream& Refrigerator::print(std::ostream& os) const {
    os << "> temperatue: " << temperature_  << std::endl
       << "> door is " << (door_open_ ? "open" : "closed") << std::endl
       << "> powered: " << (power_on_ ? "yes" : "no") << std::endl;

    return os;
}

void Refrigerator::close_door() { door_open_ = false; }
void Refrigerator::open_door() { door_open_ = true; }
void Refrigerator::switch_on() { power_on_ = true; }
void Refrigerator::switch_off() { power_on_ = false; }

void Refrigerator::set_temperature(double temperature) {
    temperature_ = temperature;
}
