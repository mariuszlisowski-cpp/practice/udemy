#include <algorithm>
#include <iostream>
#include <random>

template <typename T>
void print_random_int(T from, T to, size_t count) {
    // pseudo random number generator (PRNG)
    static std::mt19937 mt; // static because creation is time-consuming
    std::uniform_int_distribution<T> dist_int(from, to);; // inclusive range    
    for (size_t i = 0; i < count; ++i) {
        std::cout << dist_int(mt) << ' ';
    }
    std::cout << std::endl;
}

template <typename T>
void print_random_real(T from, T to, size_t count) {
    // pseudo random number generator (PRNG)
    static std::mt19937 mt; // static because avoid unintentionally resetting the sequence
    std::uniform_real_distribution<T> dist_real(from, to);
    for (size_t i = 0; i < count; ++i) {
        std::cout << dist_real(mt) << std::endl;
    }
    std::cout << std::endl;
}

int main() {

    print_random_int(10, 99, 7);
    print_random_real(0.01, 0.99, 3);

    return 0;
}
