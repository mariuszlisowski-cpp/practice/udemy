cmake_minimum_required(VERSION 3.0.0)
project(calculator VERSION 1.0.0)

add_executable(${PROJECT_NAME}
    main.cpp
    addition.cpp
    division.cpp
    print_result.cpp    
)
