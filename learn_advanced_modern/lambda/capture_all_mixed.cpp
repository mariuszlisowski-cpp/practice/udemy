#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

int main() {
    size_t base{4};
    size_t exponent{2};
    double pi{3.14};
    double degrees{360};
    
    // all captured by value
    auto functor_value = [=](){ return std::pow(base, exponent); };

    std::cout << functor_value() << std::endl;
    
    // all captured by reference (no need to use mutable)
    auto functor_reference = [&](){ return std::pow(--base, ++exponent); }; // local variables modified

    std::cout << functor_reference() << std::endl; // need to be called first (to modify local variables)
    std::cout << "> base: " << base << std::endl;
    std::cout << "> exponent: " << exponent << std::endl;

    // mixed capture (explicitly by value)
    auto functor_reference_value = [&, degrees, pi]() { 
                                                    double result = degrees / pi;   // safer capture by value
                                                    return std::pow(++base, ++exponent) + result; // modified
                                                };
    auto resultA = functor_reference_value();
    std::cout << resultA << std::endl;

    // mixed capture (explicityly by reference)
    auto functor_value_reference = [=, &base]() { 
                                                    double result = degrees / pi;   // safer capture by value
                                                    return std::pow(++base, exponent) + result; // base modified
                                                };
    auto resultB = functor_value_reference();
    std::cout << resultB << std::endl;
    
    return 0;
}
