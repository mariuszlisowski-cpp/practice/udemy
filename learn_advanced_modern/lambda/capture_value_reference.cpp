#include <iostream>

int main() {
    int number{2};

    // captured by value (const)
    auto resultA = [number]() {
                       return number * 2; // const by default
                   };
    std::cout << resultA() << " : " << number << std::endl; // number the same
    
    // captured by value (mutable)
    auto resultB = [number]() mutable {
                       return number *= 2; // can be modified (unless const outside the scope)
                   };                      // change is lost as passed by value
    std::cout << resultB() <<  " : " << number << std::endl; // number the same
    
    // captured by reference (no need to use mutable)
    auto resultC = [&number]() {
                       return number *= 2; // mutable by default (unless const outside the scope)
                   };
    std::cout << resultC() <<  " : " << number << std::endl; // number changed

    return 0;
}
