#include <iostream>
#include <string>
#include <vector>

int main() {
    std::vector<std::string> strings;
    strings.reserve(5);
    strings.push_back("one");
    strings.push_back("two");
    strings.push_back("three");

    std::cout << "> size: " << strings.size() << std::endl;

    for (size_t i = 0; i < strings.size(); ++i) {
        std::cout << strings[i] << std::endl;
    }

    std::vector<std::string>::iterator it{ strings.begin() };

    std::cout << "> first: " << *it << std::endl;

    for (auto it { strings.begin() }; it != strings.end();  ++it) {
        std::cout << *it << std::endl;
    }
    
    return 0;
}
