#include <algorithm>
#include <iostream>
#include <string>

template<class InputIt, class T>
auto find_impl(InputIt first, InputIt last, const T& value) {
    for (auto it = first; it != last; ++it) {
        if (*it == value) {
            return it;
        }
    }

    return last;
}

int main() {
    std::string str("Hello");

    // algorithm
    auto result_it = std::find(str.begin(), str.end(), 'l');
    std::cout << "> found: " << *result_it << std::endl;
    std::cout << "> at index: " << std::distance(str.begin(), result_it) << std::endl;

    // possible implementation
    result_it = find_impl(str.begin(), str.end(), 'e');
    std::cout << "> found: " << *result_it << std::endl;
    std::cout << "> at index: " << std::distance(str.begin(), result_it) << std::endl;

    return 0;
}
