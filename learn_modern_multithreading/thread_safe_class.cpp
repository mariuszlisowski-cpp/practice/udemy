#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

class Collection {
public:
    void push_back(int value) {
        m.lock();
        collection_.push_back(value);   // critical section (safe)
        m.unlock();
    }

    void print() {
        for (const auto& el : collection_) {
            std::cout << el << ' ';
        }
    }
private:
    std::vector<int> collection_;
    std::mutex m;
};

int main() {
    Collection coll;

    std::thread th1([&] {
        coll.push_back(9);
        coll.push_back(8);
        coll.push_back(7);
    });

    std::thread th2([&] {
        coll.push_back(6);
        coll.push_back(5);
        coll.push_back(4);
    });

    std::thread th3([&] {
        coll.push_back(3);
        coll.push_back(2);
        coll.push_back(1);
    });

    th1.join();
    th2.join();
    th3.join();

    coll.print();    

    return 0;
}
