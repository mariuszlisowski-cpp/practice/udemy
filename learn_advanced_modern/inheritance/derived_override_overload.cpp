#include <complex>
#include <iostream>

class Base {
  public:
    virtual ~Base() {}
    virtual void f(double) {
        std::cout << "Base class's f taking double" << std::endl;
    }
};
class Derived : public Base {
  public:
    // overridden method
    void f(double) override {
        std::cout << "Derived class's f taking double" << std::endl;
    }
    // overloaded method
    void f(std::complex<double>) {
        std::cout << "Derived class's f taking complex" << std::endl;
    }
};

int main() {
    Base base;
    Derived derived;
    std::unique_ptr<Base> base_ptr = std::make_unique<Derived>();
 
    base.f(1.0);
    derived.f(2.0);
  
    // overridden used
    base_ptr->f(3.0);   // no overloaded arguments allowed (double only)
  
    // overloaded used
    std::complex<double> cpx(3.0, 1.2);
    derived.f(cpx);

    return 0;
}
