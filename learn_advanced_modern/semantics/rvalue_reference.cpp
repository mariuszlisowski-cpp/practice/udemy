#include <iostream>
#include <string>

// accepts rvalue references only (prvalues & xvalues)
void print(std::string&& str) {
    std::cout << "> printing: " << str << std::endl; // lvalue here
}

int main() {
    /* lvalue */
    std::string&& str{std::string("test")};          // actually lvalue as a persistent instance

    std::cout << str << std::endl;
    std::cout << &str << std::endl;                  // address can be taken

    /* cannot pass lvalue to a function taking rvalue for security of an argument (which could be moved inside)
    print(str); // error */
    std::cout << str << std::endl;                   // could not exist anymore

    /* rvalue */
    print(std::string{"xvalue"});                    // expiring (temporary) string instance

    print(std::move(str));                           // passed as rvalue (instance is disposed of)
    /* instance does NOT exist anymore
    std::cout << str << std::endl; // INVALID */    
    str = "valid state";                             // safe to assign

    return 0;
}
