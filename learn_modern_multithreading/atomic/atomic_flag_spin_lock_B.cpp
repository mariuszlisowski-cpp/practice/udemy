#include <atomic>
#include <thread>
#include <iostream>
#include <vector>

using namespace std::literals;

// use atomic_flag as a simple lock
std::atomic_flag lock_cout = ATOMIC_FLAG_INIT;               // initialize flag to false

void task(int n) {
	while (lock_cout.test_and_set()) {}                      // lock the flag
	std::this_thread::sleep_for(50ms);                       // # critical
	std::cout << "> task with argument " << n << std::endl;  //   section  #
	lock_cout.clear();                                       // unlock the flag
}

int main() {
	std::vector<std::thread> threads;
	for (int i{1}; i <= 10; ++i) {
	    threads.push_back(std::thread{task, i});             // add each thread to vector and run it
    }
	for (auto& th : threads) {
        th.join();
    }
}
