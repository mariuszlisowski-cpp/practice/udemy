#include <algorithm>
#include <iostream>
#include <vector>

class is_longer_than {
public:
    is_longer_than(size_t n) : n_(n) {}
    bool operator()(const std::string& str) {
        return str.size() > n_; 
    }
private:
    size_t n_;
};

int main() {
    std::vector<std::string> words{"a", "collection" , "of", "words", "with", "varying", "lengths"};
    
    // length limit
    constexpr size_t WORD_LENGTH = 5;

    // first longer
    auto it = std::find_if(words.begin(), words.end(), is_longer_than(WORD_LENGTH));
    std::cout << "> first found: " << *it << std::endl;

    // all longer words
    std::for_each(words.begin(), words.end(),
                  [](const std::string& str) {
                      if (str.size() > WORD_LENGTH) {
                          std::cout << "> found: " << str << std::endl;
                      }
                  });

    return 0;
}
