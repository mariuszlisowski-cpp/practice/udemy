#include <iostream>
#include <memory>
#include <string>

class Base {
public:
    Base(const std::string& name) : name_(name) {
        std::cout << "# constructing base: " << name_ <<  std::endl;
    }
    ~Base() {
        std::cout << "# destructing base: " << name_ << std::endl;
    }

private:
    std::string name_;
};

class Derived : public Base {
public:
    Derived() : Base("from derived") {                                      // call base c'tor
        base_ptr = std::make_unique<Base>("initialize member");             // call base c'tor again
        std::cout << "# constructing derived" << std::endl;
    }
    ~Derived() {                                                            // NOT called
        std::cout << "# destructing derived" << std::endl;
    }

private:
    std::unique_ptr<Base> base_ptr;                                         // NOT called
};

int main() {
    /* call derived c'tor
       - which calls base c'ctor first
       - then call base c'tor initializing a pointer */
    Derived* derived{ new Derived() };
    delete derived;

    /* constructed on stack instead
    Derived derived; */

    return 0;
}
