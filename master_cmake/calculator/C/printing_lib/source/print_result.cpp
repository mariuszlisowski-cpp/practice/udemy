#include "../include/print_result.hpp"   // no need for relative path as target_include_directories exists

#include <iostream>
#include <string>

void print_result(std::string result_type, float result_value) {
    std::cout << "> " << result_type << " result:\t" << result_value << "\n";
}
