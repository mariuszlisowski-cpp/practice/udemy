#include <iostream>
#include <vector>

class Test {
public:
    Test() {
        pBuffer_ = new int[SIZE]{};
    }
    Test(const Test& other) {                                       // copy c'tor
        memcpy(pBuffer_, other.pBuffer_,
               sizeof(*pBuffer_) * SIZE);
    }
    ~Test() {
        delete[] pBuffer_;
    }

private:
    static const int SIZE{ 100 };
    int* pBuffer_;
};

Test getTest() {
    return Test();                                                  // returning by value (temporary copy)
}

// takes lvalue reference
void check(const Test& value) {
    std::cout << "> lvalue reference" << std::endl;
}

// takes rvalue reference (no need for const)
void check(Test&& value) {
    std::cout << "> rvalue reference" << std::endl;
}

int main() {
    Test test;                                                      // lvalue (has an address)

    Test& lTest{ test };                                            // lvalue reference

    Test&& rTestA{ Test() };                                         // rvalue ref. bound to rvalue (temp)
    Test&& rTestB{ getTest() };                                      // rvalue ref. bound to rvalue (temp)

    check(lTest);                                                   // lvalue version call
    check(rTestA);                                                  // lvalue version call again

    check(Test());                                                  // rvalue version call
    check(getTest());                                               // rvalue version call again

    return 0;
}
