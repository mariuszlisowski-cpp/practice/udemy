#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> vec{1, 2, 3, 4,    6, 7, 8, 9};
    constexpr size_t value = 5;

    // find first element equal to
    auto it_equal_to = std::find(vec.begin(), vec.end(), value);
    if (it_equal_to == vec.end()) {
        std::cout << value << " not found!" << std::endl;
    }
    
    // find the first element greater than
    auto it_greater_than = std::upper_bound(vec.begin(), vec.end(), value);
    std::cout << "> first greater than " << value << ": " << *it_greater_than << std::endl;
    
    std::cout << "> all greater than " << value << ":" << std::endl;
    for (auto it = it_greater_than; it != vec.end(); ++it) {
        std::cout << *it << ' ';
    }

    return 0;
}
