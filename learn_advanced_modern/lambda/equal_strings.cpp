#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

int main() {
    std::string strA{"ala"};
    std::string strB{"ALA"};

    bool are_equal = std::equal(strA.begin(), strA.end(),
                                strB.begin(),
                                [](char lch, char rch) {
                                    return std::toupper(lch) == std::toupper(rch); // case insensitive
                                });

    std::cout << "> strings are " << (are_equal ? "equal" : "NOT equal") << std::endl;

    return 0;
}
