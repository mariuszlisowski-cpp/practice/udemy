#include "concurrent_queue.hpp"

#include <iostream>
#include <future>
#include <string>

using namespace std::literals;

concurrent_queue<std::string> cq;
	
// waiting thread
void reader() {
	std::this_thread::sleep_for(1s);                                    // causes exception if reading occurs first
	std::string sdata;
	try {
		std::cout << "Reader calling pop..." << std::endl;
		auto data = cq.pop();                                           // pop the data off the queue
		std::cout << "Reader read data: " << data << std::endl;
	}
	catch (std::exception& e) {
		std::cout << "> exception caught: " << e.what() << std::endl;
	}
}

// modyifing thread
void writer() {
	std::this_thread::sleep_for(2s);                                    // causes exception if reading occurs first
	std::cout << "Writer calling push..." << std::endl;
	cq.push("entry");                                                   // push the data onto the queue
	std::cout << "Writer wrote data!" << std::endl;
}

int main() {
	auto w = async(std::launch::async, writer);
	auto r = async(std::launch::async, reader);
	r.wait();
	w.wait();
}
