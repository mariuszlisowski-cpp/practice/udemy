#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

int main() {
    // max lexicographically
    std::string strA{"ala"};
    std::string strB{"ola"};
    std::string max_lexicographically = std::max(strA, strB);
    std::cout << max_lexicographically << std::endl;

    // max length
    const char *max_length_word =
        std::max({"a", "collection", "of", "words", "with", "varying", "lengths"}, // initializer list
                 [](const std::string &lhs, const std::string &rhs) {
                     return lhs.size() < rhs.size();
                 });
    std::cout << "> longest word is: " << max_length_word << std::endl;

    // min lexicographically
    const char *cstring_min_lexi = std::min({"collection", "of", "words"});         // initializer list
    std::cout << cstring_min_lexi << std::endl;

    // min length
    const char *cstring_min_length =
        std::min({"collection", "of", "words"},                                     // initializer list
                 [](const std::string &lhs, const std::string &rhs) {
                     return lhs.size() < rhs.size();
                 });
    std::cout << "> shortest word is: " << cstring_min_length << std::endl;

    return 0;
}
