#include <iostream>
#include <map>
#include <string>

class Person {
public:
    Person() = default;
    // non-default copy constructor (deep copy)
    Person(const Person& other) : name_(other.name_), age_(other.age_) {
        std::cout << "> copy constructor" << std::endl;
    }
    Person(std::string name, int age) :
        name_(name), age_(age) {}

    void print() const {
        std::cout << name_ << ": " << age_ << std::endl;
    }

    // const reference to Person object & const this object
    bool operator<(const Person& other) const {
        if (this != &other) {
            if (name_ == other.name_) {
                return age_ < other.age_;                                           // const age_
            } else {
                return name_ < other.name_;                                         // const name_
            }
        }

        return false;                                                               // if same objects compared
    }

private:
    std::string name_;
    int age_;
};

int main() {
    std::map<Person, int> people;                                                   // map sorted by names 

    people[Person("Mike", 33)] = 1;
    people[Person("Elen", 29)] = 2;
    people[Person("Mary", 41)] = 3;
    people[Person("Mike", 32)] = 3;

    people.insert(std::make_pair(Person("Gary", 19), 4));                           // copy constructor invoke

    for (auto it{ people.begin() }; it != people.end(); ++it) {
        it->first.print();
    }

    return 0;
}
