#include <future>
#include <iostream>
#include <thread>
#include <exception>

using namespace std;

int produce() {
	int x{42};          
	this_thread::sleep_for(2s);
	throw std::out_of_range("Oops");

	return x;
}

int main() {
	auto f = async(produce);

	cout << "Future calling get()..." << endl;
	try {
		int x = f.get();				           // Get the result
		cout << "The answer is " << x << endl;
	}
	catch(exception& e) {
		cout << "Exception caught: " << e.what() << endl;
	}
}
