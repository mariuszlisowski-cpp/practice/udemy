#include <thread>
#include <mutex>
#include <iostream>

std::mutex mutex1;
std::mutex mutex2;

void func1() {
	std::cout << "Thread 1 locking mutexes 1 & 2..." << std::endl;
    std::lock(mutex1, mutex2);                                          // lock both mutexes
	std::cout << "> thread 1 has locked mutexes 1 & 2" << std::endl;

    std::unique_lock lk1(mutex1, std::adopt_lock);                      // associate each mutex
    std::unique_lock lk2(mutex2, std::adopt_lock);                      // with a unique_lock

	std::cout << "# thread 1 releases locks" << std::endl;
}

void func2() {
	std::cout << "Thread 2 locking mutexes 2 & 1..." << std::endl;
    std::lock(mutex2, mutex1);                                          // lock both mutexes (reversed order)
    std::cout << "> thread 2 has locked mutexes 2 & 1" << std::endl;

    std::unique_lock lk1(mutex1, std::adopt_lock);                      // associate each mutex
    std::unique_lock lk2(mutex2, std::adopt_lock);                      // with a unique_lock

	std::cout << "# thread 2 releases locks" << std::endl;
}

int main() {
    // deadlock prevented
	std::thread t1{ func1 };
	std::thread t2{ func2 };

	t1.join();
	t2.join();
}
