#include <iostream>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <chrono>
#include <vector>

using namespace std;
using namespace std::literals;

shared_mutex the_mutex;

void write(int i) {
	lock_guard<shared_mutex> lg(the_mutex);
	cout << "> write thread " << i << " with exclusive lock" << endl;
	this_thread::sleep_for(1s);
}

void read(int i) {
	shared_lock<shared_mutex> sl(the_mutex);
	cout << "> read thread " << i << " with shared lock" << endl;
}

int main() {
	vector<thread> threads;
	
	for (int i = 0; i < 5; ++i) {
		threads.push_back(thread{read, i});
    }

	threads.push_back(thread{write, 5});
	threads.push_back(thread{write, 6});
	
	for (int i = 0; i < 10; ++i) {
		threads.push_back(thread{read, i+7});
    }

	for (auto& t : threads) {
		t.join();
    }

    return 0;
}
