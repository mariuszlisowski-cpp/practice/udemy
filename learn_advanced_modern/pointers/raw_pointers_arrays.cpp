#include <iostream>

// pased by address (cannot be passed by value)
void print_array(int* array, size_t size) {
    for (size_t i = 0; i < size; ++i) {
        std::cout << *(array +i ) << ' ';
    }
    std::cout << std::endl;
}

// passed by address (alternative syntax)
void print_array_(int array[], size_t size) {
    for (size_t i = 0; i < size; ++i) {
        std::cout << *(array +i ) << ' ';
    }
    std::cout << std::endl;
}

// passed by reference (size known at compile time)
void print_array_ref(int (&array)[3]) { // reference to 3 ints
    for (size_t i = 0; i < 3; ++i) {
        std::cout << *(array + i) << ' ';
    }
    std::cout << std::endl;
}

int main() {
    int array[]{99, 88, 77};
    size_t size = sizeof(array) / sizeof(*array);


    print_array(array, size);       // passed by address (&array[0])
    print_array_(array, size);      // passed by address (&array[0])
    print_array_ref(array);  // passed by reference

    return 0;
}
