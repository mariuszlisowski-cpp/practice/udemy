#include "coin.hpp"

#include <iostream>

Coin::Coin(const std::string& name, const std::string& plural, int cents) :
    name_(name), plural_name(plural), cents_(cents) {}

std::string number_to_text(int number_of_coins) {
    std::string text{""};
    switch(number_of_coins) {
        case 0: text = "zero"; break;
        case 1: text = "one"; break;
        case 2: text = "two"; break;
        case 3: text = "three"; break;
        case 4: text = "four"; break;
    }

    return text;
}

int Coin::do_coin(int change) const {
    int coins{change / cents_};
    if (coins > 0) {
        std::string text = coins > 1 ? plural_name : name_;
        std::cout << (number_to_text(coins)) << ' '<< text << std::endl;
    }
    
    return change % cents_;  // remaining
}
