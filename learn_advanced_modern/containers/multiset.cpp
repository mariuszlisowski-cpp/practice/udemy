#include <iostream>
#include <set>
#include <string>

template<typename T>
void print_multiset(const std::multiset<T>& set) {
	for (const auto& el : set) {
		std::cout << el << ' ';
	}
    std::cout << std::endl;
}

int main() {
    // sorted lexicographically
	std::multiset<std::string> names {
		"Tom", "Ele", "Kim", "Kim", "Tom"
	};

	names.insert("Ele");		        // no problem
	names.insert("Alice");
    print_multiset(names);

    names.erase("Tom");                 // erase all
    print_multiset(names);

    auto it = names.find("Ele");    
    names.erase(it);                    // erase one
    print_multiset(names);

    it = names.find("Kim");             // first occurence
    auto count = names.count("Kim");    // number of occurences
    std::cout << "> " << *it << " found " << count << " times" << std::endl;
    if (it != names.end()) {
        while (count--) {
            std::cout << *it << ' ';
            ++it;
        }
    }

	return 0;
}
