#include <condition_variable>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>

using namespace std::literals;

std::mutex the_mutex;
std::condition_variable cv;
std::string shared_data{"started"};

// waiting thread
void reader() {
    std::unique_lock<std::mutex> ul(the_mutex);
    std::cout << "> read thread waiting...\n";
    cv.wait(ul);                                        // wait for condition variable to be notified
    std::cout << "> read thread has "
              << shared_data << std::endl;              // critical section (executed after waking up)
}

// modifying thread
void writer() {
    std::cout << "> writing data..." << std::endl;
    {
        std::lock_guard<std::mutex> lg(the_mutex);
        shared_data = "finished";                       // critical section
    }
    cv.notify_one();                                    // notify other thread to wake up
}

int main() {
    std::thread write(writer);                          // no threads to notify by condition variable
    std::this_thread::sleep_for(500ms);                 // lost wake up call
    std::thread read(reader);                           // waits FOREVER to be notified by write thread

    read.join();
    write.join();

    return 0;
}
