#include <iostream>

class Test {
};

template<typename T>
void call(T&& arg) {                                            // lvalue passed -> T&
    check(std::forward<T>(arg));                                // rvalue passed -> T&&
    // check(static_cast<T>(arg));                              // ...alternatively
}
// overloaded for lvalue reference argument
void check(Test& test) {
    std::cout << "> lvalue" << std::endl;
}
// overloaded for rvalue reference argument
void check(Test&& test) {
    std::cout << "> rvalue" << std::endl;
}

int main() {
    Test test;

    call(Test());                                                   // passing rvalue (temporary)
    call(test);                                                     // passing lvalue (has address)

    auto&& t{ test };                                               // collapsing to lvalue
    call(t);                                                        // passing lvalue

    return 0;
}
