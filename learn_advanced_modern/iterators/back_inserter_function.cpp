#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> vec{1, 2, 3};
    std::vector<int> rev;

    std::copy(vec.begin(), vec.end(),
              std::back_inserter(rev));

    for (const auto value : rev) {
        std::cout << value << ' ';
    }
    
    return 0;
}
