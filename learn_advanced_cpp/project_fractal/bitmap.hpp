#pragma once

#include <memory>
#include <string>
#include <cstdint>

namespace raisin {

class bitmap {
public:
    bitmap(int width, int height);
    virtual ~bitmap();

    void set_pixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue);
    bool write(const std::string& filename);

private:
    int width_{};
    int height_{};
    std::unique_ptr<uint8_t[]> pixels_{};
};

}
