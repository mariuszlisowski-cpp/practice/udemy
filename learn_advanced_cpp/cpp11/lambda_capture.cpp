#include <iostream>

int main() {
    int first{ 1 };
    int second{ 2 };

    [] { std::cout << "Hi!\n"; }();                                         // call lambda

    [first, second] { std::cout << first << second << '\n'; }();            // capture by value
    [=] { std::cout << first << second << '\n'; }();                        // capture all
    [=, &second] { std::cout << first << ++second << '\n'; }();             // first by value (read only)
                                                                            // second by reference
    [=, &second]() mutable { std::cout << ++first << ++second << '\n'; }(); // first by value (read/write)
                                                                            // second by reference
    std::cout << first << second << '\n';                                   // first not changed

    [&] { std::cout << ++first << ++second << '\n'; }();                    // all by reference
    std::cout << first << second << '\n';                                   // first changed

    [&, second] { std::cout << ++first << second << '\n'; }();              // all by ref. except second

    return 0;
}
