#include <iostream>
#include <memory>                                                   // for smart pointers

class Test {
public:
    Test() {
        std::cout << "> object created" << std::endl;
    }
    ~Test() {
        std::cout << "< object destroyed" << std::endl;
    }

    void greet() {
        std::cout << "# Hi" << std::endl;
    }
};

int main() {
    // additional scope
    {
        std::unique_ptr<Test> pTest(new Test);
        pTest->greet();
    }                                                               // object going out of scope
                                                                    // thus is detroyed
    std::cout << "> exitting" << std::endl;

    return 0;
}
