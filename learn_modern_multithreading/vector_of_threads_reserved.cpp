#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

std::mutex process_mutex;

void process(size_t id) {
	std::lock_guard<std::mutex> lg(process_mutex);						// data races prevent
	std::cout << "> thread " << id + 1 << " processed" << std::endl;	// critical section
}																		// mutex released (RAII)

int main() {
    const unsigned THREADS = 5;
    std::vector<std::thread> threads; 	// do NOT use default-constructed threads (as a count)
	threads.reserve(THREADS);			// count accepted here

    for (size_t i{0}; i < THREADS; ++i) {
        threads.push_back(std::thread(process, i));
    }
    
    for (size_t i{0}; i < threads.size(); ++i) {
        threads[i].join();
        std::cout << "> thread " << i + 1 << " joined" << std::endl;
    }

	return 0;
}
