#include <iostream>
#include <memory>                                                   // for smart pointers

class Test {
public:
    Test() {
        std::cout << "> object created" << std::endl;
    }
    ~Test() {
        std::cout << "< object destroyed" << std::endl;
    }

    void greet() {
        std::cout << "# Hi" << std::endl;
    }
};

int main() {
    std::shared_ptr<Test> pTest(nullptr);                           // NO object created
    // additional scope
    {
        std::shared_ptr<Test> pTestA{ std::make_shared<Test>() };   // creating an object 
                                                                    // instead using 'new' passing to c'tor
        pTestA->greet();

        pTest = pTestA;
    }                                                               // object going out of scope
                                                                    // but is NOT detroyed
    std::cout << "> exitting..." << std::endl;

    return 0;
}                                                                   // destroyed NOW
