#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

constexpr int SIZE{ 5 };

bool check(const std::string& str) {
    return str.size() == SIZE;
}

// functor - class defining the operator()
class Check {
public:
    bool operator()(const std::string& str) {
        return str.size() == SIZE;
    }
} check_functor;                                                                // definition of an object

// accept anything functional
void run(std::function<bool(const std::string&)> check) {                       // <return type(arguments)> name
    std::cout << std::boolalpha << check("123") << std::endl;
}

int main() {
    int size{ 3 };

    std::vector<std::string> vec{ "one", "two", "three"};                       // length of 3 & 5
    auto result = std::count_if(vec.begin(), vec.end(),
                                [size](const std::string& test) {
                                    return test.size() == size;
                                });
    std::cout << result << std::endl;

    result = std::count_if(vec.begin(), vec.end(), check);                      // function pointer
    std::cout << result << std::endl;

    result = std::count_if(vec.begin(), vec.end(), check_functor);              // functor
    std::cout << result << std::endl;

    // function accepting anything functional
    run([size](const std::string& str) -> bool {
        return str.size() == size;                                              // testing size of '123'
    });

    // functional expression
    std::function<int(int, int)> add{ [](int lhs, int rhs) {
        return lhs + rhs;
    } };
    std::cout << "> adding " << add(2, 3) << std::endl;

    std::function<bool(const std::string&)> use_functor{
        check_functor
    };
    std::cout << use_functor("54321");

    return 0;
}
