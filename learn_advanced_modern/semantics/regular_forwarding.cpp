#include <iostream>

class X {};

void g(X x) {
    std::cout << "> instance forwarded" << std::endl;
}

void f(X x) {
    g(x); // regular forwarding (not perfect)
}

int main() {
    f(X());    

    return 0;
}
