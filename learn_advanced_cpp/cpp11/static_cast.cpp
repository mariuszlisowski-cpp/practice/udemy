#include <iostream>

class Parent {
public:
    void speak() {
        std::cout << "speaking" << std::endl;
    }
};

class Brother : public Parent {
};

class Sister : public Parent {
};

int main() {
    Parent parent;
    Brother brother;

    float value{ 3.23 };
    std::cout << static_cast<int>(value) << std::endl;      // cpp11 casting

    Parent* pParent{ &brother };                            // polimorphism
    Brother* pBrother{ static_cast<Brother*>(&parent) };    // casting compulsory
                                                            // unsafe as child class may lack parent's class methods

    Parent* pPar = &brother;
    Brother* pBro = static_cast<Brother*>(pPar);

    Parent&& pP = static_cast<Parent&&>(parent);            // casting lvalue to rvalue
    pP.speak();

    return 0;
}
