#include <iostream>
#include <string>

class Parent {
public:
	Parent(): Parent("hello") {                                         // call one c'tor form another
		std::cout << "No parameter parent constructor" << std::endl;
	}
	Parent(std::string text) {                                          // c'tor called above
		std::cout << "Parametrized parent constructor" << std::endl;
	}
    
    ~Parent() {
		std::cout << "Parent destructor" << std::endl;
    }

private:
	int dogs{5};
	std::string text{"hello"};
};

class Child: public Parent {
public:
    Child() {
		std::cout << "No parameter child constructor" << std::endl;
    }
    ~Child() {
		std::cout << "Child destructor" << std::endl;
    }
};

int main() {
	Parent parent("Hello");
    std::cout << std::endl;

	Child child;                                                        // two parent c'ctors called
                                                                        // and child c'ctor
    std::cout << std::endl;

	return 0;
}
