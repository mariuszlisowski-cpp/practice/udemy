#include <algorithm> // std::copy
#include <cstddef>   // std::size_t
#include <iostream>

class dumb_array {
  public:
    // (default) constructor
    dumb_array(std::size_t size = 0) : mSize(size), mArray(mSize ? new int[mSize]() : nullptr) {}

    // destructor
    ~dumb_array() { delete[] mArray; }
    // copy-constructor (non-throwing because of the data types being used)
    dumb_array(const dumb_array& other) : mSize(other.mSize), mArray(mSize ? new int[mSize] : nullptr) {
        std::copy(other.mArray, other.mArray + mSize, mArray);
    }
    // copy assignment operator
    dumb_array& operator=(dumb_array& other) {
        swap(*this, other);
        return *this;
    }

    friend void swap(dumb_array& first, dumb_array& second);


    /* WORKING (strong exception guarantee) but code duplication */
    dumb_array& operator=(const dumb_array& other) {
        if (this != &other) {
            // get the new data ready before we replace the old
            std::size_t newSize = other.mSize;
            int* newArray = newSize ? new int[newSize]() : nullptr;
            std::copy(other.mArray, other.mArray + newSize, newArray);

            // replace the old data (all are non-throwing)
            delete [] mArray;
            mSize = newSize;
            mArray = newArray;
        } 

        return *this;
    }

    /* FAILED (basic exception guarantee only)
    dumb_array& operator=(const dumb_array& other) {
        if (this != &other) {
            // get rid of the old data...
            delete [] mArray;
            mArray = nullptr;                               // if 'new' below fails, *this will have been modified

            // ...and put in the new
            mSize = other.mSize; // (3)
            mArray = mSize ? new int[mSize] : nullptr; // (3)
            std::copy(other.mArray, other.mArray + mSize, mArray); // (3)
        }

        return *this;
    } */

  private:
    std::size_t mSize;
    int* mArray;
};

// global and friend of a class
void swap(dumb_array& first, dumb_array& second) {
    // enable ADL (not necessary in our case, but good practice)
    using std::swap;

    // by swapping the members of two objects, the two objects are effectively swapped
    swap(first.mSize, second.mSize);
    swap(first.mArray, second.mArray);
}

int main() {
    dumb_array arrA(5);
    dumb_array arrB(5);

    arrA = arrB;

    return 0;
}
