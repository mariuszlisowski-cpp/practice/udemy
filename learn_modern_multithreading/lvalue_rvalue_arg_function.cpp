#include <iostream>

void function(const int& value) {
    std::cout << "LValue reference passed: " << value << std::endl;
}

void function(const int&& value) {
    std::cout << "RValue reference passed: " << value << std::endl;
}

int main() {
    int a = 9;

    function(a);
    
    function(std::move(a));
    function(2);

    return 0;
}
