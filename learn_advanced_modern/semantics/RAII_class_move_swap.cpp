#include <iostream>

class BufferManager {
public:
    BufferManager(size_t size) : size_(size) {}
    BufferManager(const BufferManager& ) = delete;           // copy constructor disabled (fallback to move c'ctor)
    BufferManager& operator=(const BufferManager&) = delete; // copy assignment disabled (fallback to move assignment)

    // move constructor
    BufferManager(BufferManager&& other) noexcept {
        std::cout << "> move constructor" << std::endl;
        // global function used
        swap(*this, other);         // after swapping brand new other.this pointer is unassigned
                                    // thus deleting it by destructor will cause undefined behaviour
        other.buffer_ = nullptr;    // MUST: can be safely destructed now (deleting nullptr has no effect)
    }                               // other is an old this pointer destructed here (out of scope)

    // move assignment operator
    BufferManager& operator=(BufferManager&& other) noexcept {
        std::cout << "> move assignment" << std::endl;
        if (this != &other) {
            // global function used
            swap(*this, other);     // after swapping other.this pointer assigned to its old value
                                    // thus no need to set it to nullptr (safe to delete)
        }
        
        return *this;               // return assinged-to instance
    }

    friend void swap(BufferManager& lhs, BufferManager& rhs);

    size_t get_size() {
        return size_;
    }

private:
    size_t size_;
    char* buffer_;                  // pointer member (care to be taken with move constructor)
};

// if arguments are class instances its swap version will be used instead of generic standard version
void swap(BufferManager& lhs, BufferManager& rhs) {
    using std::swap;                    // used for generic version
    swap(lhs.size_, rhs.size_);         // class instance version or generic (build-in types)
    swap(rhs.buffer_, rhs.buffer_);
}

int main() {
    BufferManager managerA{std::move(BufferManager{1})};                        // move construtor
    std::cout << "> buffer size: " << managerA.get_size() << std::endl;

    BufferManager managerB{0};
    managerB = BufferManager{2};                                                // move assignment
    std::cout << "> buffer size: " << managerA.get_size() << std::endl;

    /* no copying
    BufferManager managerC{3};
    managerB = managerC; // ERROR */

    return 0;
}
