#ifndef USED_CAR_HPP
#define USED_CAR_HPP

#include "../inc/car.hpp" // CMake: no need for relative path as include directory known (IDE compliant)
#include <string>

class Used_car : public Car {
public:
    Used_car(const std::string& make, const std::string& model, unsigned int year, unsigned mileage);
    
    friend std::ostream& operator<<(std::ostream& os, const Used_car& car);
};

#endif
