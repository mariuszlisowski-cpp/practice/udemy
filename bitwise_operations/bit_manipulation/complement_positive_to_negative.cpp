#include <bitset>
#include <iostream>

int main() {
    /* two's complement:
         5  0101
        ...
         0  0000
        -1  1111
         ...
        -5  1011
            ^       sign bit
    */

    char positive = 5;
    std::bitset<4> binary_positive(positive);
    std::cout << (int)(positive) << '\t'
              << binary_positive << std::endl;

    /* 1st step 
       invert using bitwise NOT operator (~) which
       performs one's complement of any binary number as argument
    */
    char negative = ~positive;                                      // 1's complement

    /* 2nd step
       plus ONE to the result of the previous step
    */
    negative += 1;                                                  // 2's complement
    
    std::bitset<4> binary_negative(negative);
    std::cout << (int)(negative) << '\t'
              << binary_negative << std::endl;

    return 0;
}
