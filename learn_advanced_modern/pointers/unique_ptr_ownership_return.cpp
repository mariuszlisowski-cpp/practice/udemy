#include <iostream>
#include <memory>

template <typename T>
std::unique_ptr<T> create_unique_ptr(const T& value) {
    std::unique_ptr<T> u_ptr{std::make_unique<T>(value)}; // created locally

    return u_ptr; // ownership returned to caller (no need to move - RVO in operation)
}

int main() {
    // factory pattern
    auto u_ptr_int{create_unique_ptr(77)}; // ownership transferred from the function
    auto u_ptr_double{create_unique_ptr(3.14)};

    std::cout << *u_ptr_int << std::endl;
    std::cout << *u_ptr_double << std::endl;



    return 0;
}
