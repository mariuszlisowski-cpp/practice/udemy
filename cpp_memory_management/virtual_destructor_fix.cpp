#include <iostream>
#include <memory>
#include <string>

class Base {
public:
    Base(const std::string& name) : name_(name) {
        std::cout << "# constructing base: " << name_ <<  std::endl;        // 1 & 2
    }
    virtual ~Base() {                                                       // MUST BE virtual
        std::cout << "# destructing base: " << name_ << std::endl;          // 5 & 6
    }

private:
    std::string name_;
};

class Derived : public Base {
public:
    Derived() : Base("from derived") {                                      // call base c'tor
        base_ptr = std::make_unique<Base>("initialize member");             // call base c'tor again
        std::cout << "# constructing derived" << std::endl;                 // 3
    }
    ~Derived() {                                                            // CAB BE virtual if further derived
        std::cout << "# destructing derived" << std::endl;                  // 4
    }

private:
    std::unique_ptr<Base> base_ptr;
};

int main() {
    /* call derived c'tor
       - which calls base c'ctor first
       - then call base c'tor initializing a pointer */
    Base* base_ptr{ new Derived() };
    delete base_ptr;

    /* alternatively
    std::unique_ptr<Base> u_base_ptr{ std::make_unique<Derived>() }; */

    return 0;
}
