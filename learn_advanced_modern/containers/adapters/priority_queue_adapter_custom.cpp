#include <iostream>
#include <queue>
#include <vector>

class Record {
public:
    Record(int count) : count_(count) {}
    int get_count() const {
        return count_;
    }
    bool operator<(const Record& rhs) const {
        if (this != &rhs) {
            return this->count_ > rhs.count_;
        }

        return false;
    }
private:
    int count_;
};

template<typename T>
void pop_queue(std::priority_queue<T, std::vector<T>>& priority_q) {
    while (priority_q.size()) {
        T record = priority_q.top();
        std::cout << record.get_count() << ' ';
        priority_q.pop();
    }
    std::cout << std::endl;
}

int main() {
    // usually implemented as a std::vector (redundant here)
    std::priority_queue<Record, std::vector<Record>> priority_q; // custom comparator
    priority_q.push(Record(4));
    priority_q.push(Record(5));
    priority_q.push(Record(3));
    priority_q.push(Record(1)); // highest priority
    priority_q.push(Record(2));

    Record top_record = priority_q.top();    
    std::cout << "> highest prioroty: " << top_record.get_count() << std::endl;

    priority_q.pop(); // remove front element

    std::cout << "> size: " << priority_q.size() << std::endl;

    pop_queue(priority_q);
        
    if (priority_q.empty()) {
        std::cout << "> queue is empty now" << std::endl;
    }

    return 0;
}
