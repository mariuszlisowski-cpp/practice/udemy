#include <iostream>
#include <string>

class Test {
public:
    Test(int id, std::string name) : id_(id), name_(name) {
        ptr_ = new int(99);
    }
    // destructor (rule of three)
    ~Test() {
        delete ptr_;
    }
    // copy assignment operator (rule of three)
    Test& operator=(const Test& other) {
        std::cout << "> custom assignment operator"  << std::endl;
        if (this != &other) {
            id_ = other.id_;
            *ptr_ = *other.ptr_;                                    // deep copy (reuse of object's memory)
            name_ = other.name_;                                    // thus no need to delete ptr
        }

        return *this;
    }
    /* to adhere to the rule of three a copy constructor
        should be implemented here */

    void print_data() const {
        std::cout << id_ << ": " << name_ << std::endl;
    }
    void print_pointer() const {
        std::cout << "pointer value: " << *ptr_ << ", ";            // pointer value (same)
        std::cout << "pointing to: " << ptr_ << ", ";               // address inside the pointer (different)
        std::cout << "pointer address: " << &ptr_ << std::endl;     // address of the pointer (different)
    }

private:
    int id_;
    int* ptr_;
    std::string name_;
};

int main() {
    Test testA(77, "Me");
    Test testB(36, "She");

    testB = testA;                                                  // custom assignment operator (deep copy)
    testB.operator=(testA);                                         // actual call

    testA.print_data();
    testB.print_data();                                             // data deep copied

    testA.print_pointer();                                          // pointer deep copied (different pointers
    testB.print_pointer();                                          // pointing to the different address - OK)

    Test testC = testA;                                             // implicit copy constructor

    return 0;
}
