#include <iostream>

class WithPointer {
  public:
    WithPointer(int n) : n_(n) {
        p_ = new char[n];
    }
    ~WithPointer() {
        delete[] p_;
    }

    /* compiler generated copy constructor makes shallow copy
       WithPointer(const WithPointer& other) : p_(other.p_), n_(other.n_) {} // pointers refer to the same address */

    // copy constructor making deep copy
    WithPointer(const WithPointer& other) : n_(other.n_) {
        p_ = new char[n_]; // already assigned above
        for (size_t i = 0; i < n_; ++i) {
            p_[i] = other.p_[i];
        }
    }

    /* compiler generated assignment operator makes shallow copy
    WithPointer& operator=(const WithPointer& other) {
        p_ = other.p_; // memory leak (previous memory is lost)
        n_ = other.n_;
        return *this;
    } */;

    // assignment operator makes deep copy
    WithPointer& operator=(const WithPointer& other) {
        // important to check self-assignment
        if (this != &other) {
            n_ = other.n_;
            delete [] p_;       // memory releas
            p_ = new char[n_];
            for (size_t i = 0; i < n_; ++i) {
                p_[i] = other.p_[i];
            }
        }

        return *this;           // returning modified instance
    }

    char* get_ptr() const {
        return p_;
    }

  private:
    char* p_;
    int n_;
};

void print_array(const WithPointer& w_ptr, size_t size) {
    for (size_t i = 0; i < size; ++i) {
        std::cout << *(w_ptr.get_ptr() + i) << ' ';
    }
    std::cout << std::endl;
}

int main() { 
    size_t array_size = 9;
    WithPointer w_ptrA(array_size);

    // populate
    for (size_t i = 0; i < array_size; ++i) {
        *(w_ptrA.get_ptr() + i) = (char)(i + 97);
    }
    print_array(w_ptrA, array_size);

    // copy
    WithPointer w_ptrB(w_ptrA); // calling copy constructor
    print_array(w_ptrB, array_size);

    // assign
    WithPointer w_ptrC(0);
    w_ptrC = w_ptrB;
    print_array(w_ptrC, array_size);

    return 0;
}
