#include <bitset>
#include <climits>
#include <iostream>
/*          0xff 
    number  00000000 11111111   loop 1 (index 0)
    number  00000001 11111110   number <<= 1 (add 0 to the right)
    number  00000011 11111100   ...
    number  00000111 11111000
    number  00001111 11110000
    number  00011111 11100000
    number  00111111 11000000
    number  01111111 10000000
    number  11111110 00000000
    number  11111100 00000000
    number  11111000 00000000
    number  11110000 00000000
    number  11100000 00000000
    number  11000000 00000000
    number  10000000 00000000   ...
    number  00000000 00000000   loop 16 (index 15)
            &
    mask    10000000 00000000 
    result '0' if (number & mask == 0) or ...
           '1' if (number & mask != 0) or (number & mask > 0)
 */

void print_binary(uint16_t number) {
    /* set most significant bit to 1 */
    uint16_t mask = 1 << (sizeof(uint16_t) * CHAR_BIT - 1);                    // move 15 places to right (2 * 8 - 1)

    /* verbose */
    std::cout << "mask: " << std::bitset<sizeof(number) * CHAR_BIT>(mask)
              << " | result: ";

    for(auto i = 0; i <  sizeof(uint16_t) * CHAR_BIT; ++i) {              // 16 times loop (2 * 8)
        std::cout << ((number & mask) != 0 ? '1' : '0');
        /* optional */
        if ((i + 1) % 8 == 0) {                                                // space every 8th bit
            std::cout << ' ';
        }

        number <<= 1;                                                           // move 1 position to the left (add 0)
    }
    std::cout << std::endl;
}

int main() {
    print_binary(0xff);
    print_binary(0x0f);
    print_binary(42u);

    return 0;
}
