#include <iostream>
#include <new>


class can_go_wrong {
public:
    can_go_wrong() {
        pMemory = new char[BYTES_ALLOCATED];
    }

    ~can_go_wrong() {
        delete [] pMemory;
    }

private:
    unsigned long const BYTES_ALLOCATED{999999999999999};               // bad alloc
    char* pMemory;
};

int main() {
    try {
        can_go_wrong wrong;
    } catch (std::bad_alloc& e) {
        std::cout << "> cannot allocate memory: " << e.what() << std::endl;
    }

    std::cout << "> exitting normally" << std::endl;


    return 0;
}
