#include "../include/addition.hpp"      // no need for relative path as target_include_directories exists

float addition(float num1, float num2) { return num1 + num2; }
