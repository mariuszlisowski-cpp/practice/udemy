#include <iostream>
#include <string>

int main() {
    char cstringA[] {'H', 'i', '\0'};   // must be terminated
    char cstringB[] {"Hi there"};       // string literal (termination added by a compiler)
    const char* cstringC = "Hi you";    // string literal (const a must to be safe)
    char* cstringD = (char*) "Hello";   // or explicit casting
    
    char* pointerA = &cstringA[0];      // cstringA is the same as pointerA
    char* pointerB = cstringA;          // same as PointerA
    
    std::string str("string");
    const char* cstringE = str.c_str(); // points to str

    std::cout << cstringA << std::endl;
    std::cout << cstringB << std::endl;
    std::cout << cstringC << std::endl;
    std::cout << cstringD << std::endl;
    std::cout << cstringE << std::endl;

    std::cout << pointerA << std::endl;
    std::cout << pointerB << std::endl;

    return 0;
}
