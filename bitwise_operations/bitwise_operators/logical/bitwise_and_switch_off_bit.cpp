/* switch OFF a single bit - symbol & */

#include <bitset>
#include <cstdint>
#include <iostream>

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1

   value  00000111
          AND
   mask   11111101    1st bit
   result 00000101    1st is OFF

   truth table:
   0 AND 0 -> 0
   0 AND 1 -> 0
   1 AND 0 -> 0
   1 AND 1 -> 1       both true -> 1
*/

int main() {
    uint8_t value{0b00000111};                  // switch OFF 1st bit (zero-indexed)
    uint8_t mask_{0b11111101};                  // mask of 1st bit

    uint8_t result = value & mask_;             // first operand bit NOT changed if 1 in mask

    std::cout << std::bitset<8>(result)
              << std::endl;

    return 0;
}
