#include <string>

/*  The rule of three:
    If you need to explicitly declare either 
    - the destructor
    - copy constructor
    - copy assignment operator
    you probably need to explicitly declare all three of them
*/

class person {
  public:
    person(const std::string& name, int age);       // Constructor

    ~person() noexcept = default;                   // 1/5: Destructor
    person(const person&) = default;                // 2/5: Copy Constructor
    person& operator=(const person&) = default;     // 3/5: Copy Assignment Operator

  private:
    std::string name;
    int age;
};

int main() { return 0; }
