#include <bitset>
#include <iostream>

int main() {
    //               |   16 bits    ||               |
    auto number0 = 0b11111111111111111111111011111111;
    //                                     9876543210
    auto number1 = 0b00000000000000000000000100000000;
    auto position{8u};

    /* without mask */
    std::cout << std::bitset<1>((number0 >> position) & 1u) << ' ';
    std::cout << ((number0 >> position) & 1u) << std::endl;

    /* with mask */
    decltype(number0) bitMask {};
    bitMask |= 1u << position;

    // auto bitMask = 0b10000000; // 0x80
    std::cout << std::bitset<1>((number1 & bitMask) >> position) << ' ';
    std::cout << ((number1 & bitMask) >> position) << std::endl;

    return 0;
}
