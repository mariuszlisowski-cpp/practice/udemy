#include <iostream>
#include <map>
#include <string>

int main() {
    std::map<char, std::map<int, std::string>> nested_map {
        {'a', { 
                {1, "aa"},
                {2, "bb"}, 
                {3, "cc"} 
              }
        },
        {'b', {
                {4, "dd"},
                {5, "ee"},
                {6, "ff"}
              }
        }
    };

    // c++17
    for (auto& [ch, map] : nested_map) {
        std::cout << ch << " : "; 
        for (auto& [val, str] : map) {
            std::cout << "[" << val << " : " << str << "] ";
        }
        std::cout << std::endl;
    }

    return 0;
}
