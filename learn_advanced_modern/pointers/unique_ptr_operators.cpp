#include <iostream>
#include <memory>
#include <ostream>

struct Point {
    int x;
    int y;

};

// no need for friendship with struct (as data members are public by default) 
std::ostream& operator<<(std::ostream& os, const Point& point) {
    os << "Point(" << point.x << ", " << point.y << ")";
    return os;
}

int main() {
    std::unique_ptr<Point> uptr{std::make_unique<Point>(Point{4, -3})}; // or auto uptr ...

    // operators usage
    if (!uptr) {
        std::cout << "> nullptr" << std::endl;
    } else {
        std::cout << uptr->x << " : " << uptr->y << std::endl;
        std::cout << *uptr << std::endl;
    }

    /* copying not allowed
    auto uptr_copy = uptr;
    */

    /* arithmetic not supported
    uptr++; 
    uptr + 1;*/

    return 0;
}
