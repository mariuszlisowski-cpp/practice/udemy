#include <iostream>
#include <memory>
#include <string>


class Dog {
public:
    Dog(std::string name) : name_(name) {
        std::cout << "> a dog object is created" << std::endl;
    }
    ~Dog() {
        std::cout << "> a dog object is destroyed" << std::endl;
    }

    void make_friend(const std::shared_ptr<Dog>& dog) {
        dog_ = dog;
        std::cout << name_ << " is " << dog->name_  << "'s friend" << std::endl;
    }

    std::string get_name() {
        return name_;
    }

private:
    std::string name_;
    std::shared_ptr<Dog> dog_;
};

int main() {
    std::shared_ptr<Dog> dogA{std::make_shared<Dog>("Nero")}  ;
    std::shared_ptr<Dog> dogB{std::make_shared<Dog>("Rex")}  ;

    dogA->make_friend(dogB);
    dogB->make_friend(dogA); // memory leak (pointer will never go out of scope)

    std::cout << "> pointer A count: " << dogA.use_count() << std::endl;
    std::cout << "> pointer B count: " << dogB.use_count() << std::endl;

    return 0;
}
