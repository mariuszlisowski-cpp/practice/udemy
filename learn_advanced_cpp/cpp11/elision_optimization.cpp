#include <iostream>
#include <vector>

class Test {
public:
    Test() {
        std::cout << "constructor" << std::endl;
        pBuffer_ = new int[SIZE]{};                                         // zeroed array
    }
    Test(int val) {
        std::cout << "parameterized constructor" << std::endl;
        pBuffer_ = new int[SIZE]{};                                         // code repetition
                                                                            // default c'tor should be called
        for (int i = 0; i < SIZE; i++) {
            pBuffer_[i] = val;
        }
    }
    Test(const Test& other) {
        std::cout << "copy constructor" << std::endl;
        pBuffer_ = new int[SIZE]{};                                         // code repetition
                                                                            // default c'tor should be called
        memcpy(pBuffer_, other.pBuffer_, sizeof(*pBuffer_) * SIZE);         // dereference then multiply
    }

    Test& operator=(const Test& other) {
        std::cout << "assignment operator" << std::endl;
        pBuffer_ = new int[SIZE]{};                                         // code repetition
        memcpy(pBuffer_, other.pBuffer_, sizeof(*pBuffer_) * SIZE);         // copy c'tor should be called instead

        return *this;
    }

    ~Test() {
        delete[] pBuffer_;
    }

    friend std::ostream& operator<<(std::ostream& out, const Test& test);

private:
    static const int SIZE{ 100 };
    int* pBuffer_;
};

std::ostream& operator<<(std::ostream& out, const Test& test) {
    out << "Hello from test";
    return out;
}

Test getTest() {
    return Test();                                                          // returning by value
                                                                            // should return a copy
}

int main() {
    Test test = getTest();                                                  // copy elision (copying omitted)
                                                                            // no copy constructor called
    std::cout << test << "\n\n";

    std::vector<Test> vec;
    vec.push_back(Test());

    return 0;
}
