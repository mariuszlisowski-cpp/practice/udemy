#include <iostream>
#include <memory>

template< typename T >
struct array_deleter {
	void operator ()( T const * p) { 
	delete[] p; 
}
};

int main() {
	// custom deleter
	std::shared_ptr<int> sp_deleter(new int[10], array_deleter<int[]>());
	std::shared_ptr<int> sp_lambda(new int[10], [](int *p) {
							    delete[] p; // in-place deleter
							});
	
	// partial specialization for array types
    std::shared_ptr<int> assd(new int[16], std::default_delete<int[]>());

	std::unique_ptr<int[]> up(new int[10]); // this will correctly call delete[]

    return 0;
}
