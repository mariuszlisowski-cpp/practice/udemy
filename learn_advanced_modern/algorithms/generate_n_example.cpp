#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

template <class T> void print_container(const std::vector<T>& cont) {
    std::for_each(cont.begin(), cont.end(),
                  [](const T& s) { std::cout << s << ' '; });
    std::cout << std::endl;
}

int main() {
    constexpr size_t size = 11;
    std::vector<int> v(size);

    // popoulate half the vector
    auto it = std::generate_n(v.begin(), v.size() / 2, [n = 0]() mutable { return n++; });

    // populate second half (using obtained iterator)
    std::generate(it, v.end(), [n = 5]() mutable { return n--; });

    print_container(v);

    return 0;
}
