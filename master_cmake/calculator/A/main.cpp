#include "addition.hpp"
#include "print_result.hpp"
#include "division.hpp"

int main() {
    float first_no = 10;
    float second_no = 5;
    float result_add, result_div;

    result_add = addition(first_no, second_no);
    result_div = division(first_no, second_no);

    print_result("addition", result_add);
    print_result("division", result_div);

    return 0;
}
