/* power of 2 */

#include <bitset>
#include <cstdint>
#include <iostream>

/*                integer_to_shift   <<   no_of_positions (unsigned)
        MSB is discarded per shift   <<   LSB is replaced by 0

                    1 0 0 0 0 0 0 1  <<   1 (one left)
    1 discarded  <- 0 0 0 0 0 0 1 0  <-   0 added
                    ^             ^
                  MSB             LSB
    most significant bit  |  least significant bit

    00000001 << 0 -> 00000001        1      2^0
    00000001 << 1 -> 00000010        2      2^1
    00000001 << 2 -> 00000100        4      2^2
    00000001 << 3 -> 00000100        8      2^3
    00000001 << 4 -> 00001000       16      2^4
    00000001 << 5 -> 00010000       32      2^5
    00000001 << 6 -> 01000000       64      2^6
    00000001 << 7 -> 10000000      128      2^7
*/

void print(uint8_t val) {
    std::cout << int(val) << '\t'
             << std::bitset<8>(val) << std::endl;
}

int main() {
    constexpr auto byte{8u};

    for (auto i{0}; i < byte; ++i) {                    // 0 - 7
        uint8_t result = 1u << i;                            // std::pow(2, i)
        print(result);
    }

    return 0;
}
