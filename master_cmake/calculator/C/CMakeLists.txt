cmake_minimum_required(VERSION 3.0.0)
project(calculator VERSION 1.0.0)

# 1.1 build library target
add_subdirectory(calculations_lib)                                  # looking for CMakeLists.txt there
# now read from subdirectory
# add_library(calculations addition.cpp division.cpp)

# 1.2 build library target
add_subdirectory(printing_lib)                                      # looking for CMakeLists.txt there
# now read from subdirectory
# add_library(printing print_result.cpp)

# 1.3 build executable target
add_executable(${PROJECT_NAME}
    main.cpp
)

# 2.0 link executable with libraries (dependencies)
target_link_libraries(${PROJECT_NAME} PRIVATE calculations printing) # executable USES both libraries (not vice versa)

# SCOPE OPERATORS:
#
# When A links in B as PRIVATE, it is saying that A uses B in its
# implementation, but B is not used in any part of A's public API                               A -> B
#                                                                    
# When A links in B as INTERFACE, it is saying that A does not use B
# in its implementation, but B is used in A's public API                                        A <- B
#
# When A links in B as PUBLIC, it is essentially a combination of PRIVATE and INTERFACE
# It says that A uses B in its implementation and B is also used in A's public API              A <-> B
