#include <iostream>

class Vehicle {
public:
    void parent_chilren() {
        std::cout << "> engine started" << std::endl;
    }

protected:
    void children_only() {
        std::cout << "> only for children" << std::endl;
    }

private:
    void base_class_access_only() {};
};

class Aeroplace : public Vehicle {
public:
    void child() {
        children_only(); // protected parent class allowed only here
    }
};


int main() {
    // parent class
    Vehicle vehicle;
    vehicle.parent_chilren();
    
    /* can only be called by children of this class
    vehicle.children_only() */

    // child class
    Aeroplace plane;
    plane.parent_chilren();
    
    /* not accessible outside child class as it's protected
    plane.children_only(); */ 
    plane.child();

    return 0;
}
