#include <fstream>
#include <iostream>

int main() {
    std::ofstream out_stream;

    out_stream.open("hello", std::ofstream::binary); // binary mode (std::ios_base::binary)

    if (out_stream.is_open()) {
        out_stream << "\xff\xfe\xfd";

        out_stream.close();
    }

    return 0;
}

