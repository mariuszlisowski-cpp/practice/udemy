#include <cctype>
#include <iostream>
#include <string>

int main() {
    std::string str("HHello, world!!!", 1, 13);

    std::cout << str << std::endl;

    for (const auto& ch : str) {
        if (std::isupper(ch)) {
            std::cout << ch << " is uppercase" << std::endl;
        }
        if (std::islower(ch)) {
            std::cout << ch << " is lowercase" << std::endl;
        }
        if (std::ispunct(ch)) {
            std::cout << ch << " is punctuation" << std::endl;
        }
        if (std::isspace(ch)) {
            std::cout << ch << " is whitespace" << std::endl;
        }
    }

    return 0;
}
