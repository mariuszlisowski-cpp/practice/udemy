cmake_minimum_required(VERSION 3.10)
project(dealer)

set(BOOST_DIR /Library/Developer/CommandLineTools/usr/include/c++/user) # variable (boost path)
set(INCLUDE_DIR ${CMAKE_SOURCE_DIR}/inc)                                # variable (headers path)
set(SOURCE_DIR ${CMAKE_SOURCE_DIR}/src)                                 # variable (sources path)
set(SOURCES                                                             # variable (sources list)
    ${SOURCE_DIR}/car.cpp
    ${SOURCE_DIR}/used_car.cpp
)                                                       

include_directories(${INCLUDE_DIR})                                     # avoiding relative include path (optional)
include_directories(${BOOST_DIR})                                       # including boost headers 

add_library(${PROJECT_NAME}-lib STATIC ${SOURCES})                      # adding sources as library


add_executable(${PROJECT_NAME} ${CMAKE_SOURCE_DIR}/main.cpp)            # adding entry source (target)

target_link_libraries(${PROJECT_NAME} PRIVATE ${PROJECT_NAME}-lib)      # linking target with libraries
