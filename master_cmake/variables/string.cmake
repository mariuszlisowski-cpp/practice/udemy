# script mode: cmake -P <filename>
cmake_minimum_required(VERSION 3.0.0)

# string(<subcommand> <list_name> <args> <return_variable>)              
# e.g. APPEND, FIND, PREPEND, TOLOWER, TOUPPER, COMPARE

set(VAR "CMake for Cross-Platform C++ Projects")

string(FIND ${VAR} "for" FIND_VAR)
message(${FIND_VAR})                                        # found at index

string(FIND ${VAR} "For" FIND_VAR)
message(${FIND_VAR})                                        # -1 (not found)

string(REPLACE "Projects" "Project" REPLACE_VAR ${VAR})
message(${REPLACE_VAR})

string(PREPEND REPLACE_VAR "Master ")
message(${REPLACE_VAR})

string(APPEND REPLACE_VAR " Building")
message(${REPLACE_VAR})

string(TOLOWER ${REPLACE_VAR} LOWER_CASE_VAR)
message(${LOWER_CASE_VAR})

string(TOUPPER ${REPLACE_VAR} UPPER_CASE_VAR)
message(${UPPER_CASE_VAR})

SET(CPP "C++")
string(COMPARE EQUAL ${CPP} "C++" EQUALITY_VAR)
message(${EQUALITY_VAR})                                    # 1 if the same (otherwise -1)
