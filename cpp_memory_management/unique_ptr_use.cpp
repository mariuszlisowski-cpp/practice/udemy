/* ways to create unique pointer */
#include <memory>
#include <iostream>

struct Foo {
    Foo() {}
    Foo(int length, double radius)
        : length_(length), radius_(radius) {}

    int length_{};
    double radius_{};
};

struct Bar {};

int main() {
    std::unique_ptr<Foo> u_ptr{ std::make_unique<Foo>() };

    Foo* raw = u_ptr.get();                                     // get raw ptr
    auto length{ u_ptr->length_ };                              // use struct members
    u_ptr.reset();                                              // delete memory
    if (!u_ptr) {                                               // nullptr
        std::cout << "> object deleted" << std::endl;
    }
    if (raw) {                                                  // raw still exist but is invalid
        std::cout << "> invalid pointer!" << std::endl;
    }
    raw = nullptr;                                              // be SAFE!

    u_ptr.reset(new Foo());                                     // reuse existing ptr
    raw = u_ptr.release();                                      // releses the ownershib (NOT deleted)
    if (raw) {
        std::cout << "> valid pointer" << std::endl;
    }
    if (!u_ptr.get()) {
        std::cout << "> nullptr" << std::endl;
    }
    return 0;
}
