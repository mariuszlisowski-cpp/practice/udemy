#include <iostream>
#include <iterator>
#include <fstream>
#include <vector>

using namespace std;

class Refrigerator {
public:
    Refrigerator(double temperature, bool door = true, bool power = false) :
        temperature_(temperature), door_open_(door), power_on_(power) {}

	void print(ostream& os) const;

private:    
    double temperature_;
    bool door_open_;
    bool power_on_;
};

void Refrigerator::print(std::ostream& os) const {
    os << "> temperature " << temperature_ << ", "
       << "door is " << (door_open_ ? "open" : "closed") << ", "
       << "power is " << (power_on_ ? "on" : "off");
}

std::ostream& operator <<(std::ostream & os, const Refrigerator& fridge) {
    fridge.print(os);

    return os;
}

int main() {
	Refrigerator fridgeA(21.7);
	Refrigerator fridgeB(1.7, false, true);
	Refrigerator fridgeC(4.9, false, true);

    std::vector<Refrigerator> fridges {fridgeA, fridgeB, fridgeC};

    // output to a console
	std::ostream_iterator<Refrigerator> os_it_cout(cout, "\n"); // may be separated by any cstring
	for (const auto& fridge : fridges) {
        *os_it_cout = fridge; // no need to increment (will be pushed onto the stream after each iteration)
    }
	
    // output to a file
	std::ofstream ofile("fridges");
    if (ofile) {
        std::ostream_iterator<Refrigerator> os_it_file(ofile, "\n"); // may be separated by any cstring
        for (const auto& fridge : fridges) {
            *os_it_file = fridge; // no need to increment (will be pushed onto the stream after each iteration)
        }
        ofile.close();
        std::cout << "> fridges logged" << std::endl;
    }

    return 0;
}
