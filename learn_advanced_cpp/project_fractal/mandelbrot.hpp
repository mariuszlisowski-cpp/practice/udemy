#pragma once

#include <complex>

namespace raisin {

class mandelbrot {
public:
    static const int MAX_ITERATIONS{ 1000 };
    static int get_iterations(double x, double y);
};

}
