#include <climits>
#include <iostream>
#include <limits>

/* most significant bit (msb) indicates a sign in signed types */
auto get_num_of_bits_without_msb(auto value) {
    return std::numeric_limits<decltype(value)>::digits;                              // returns without most
}                                                                                     // significant bit if signed type

auto get_num_of_bits(auto value) {
    return sizeof(decltype(value)) * CHAR_BIT;                                        // nuumber of bytes
           // std::numeric_limits<unsigned char>::digits;                             // byte size of char
}                                                                                     // is guaranteed to be 1 byte

int main() {
    std::cout << get_num_of_bits_without_msb('a') << std::endl;                 // char (signed)    -> 7
    std::cout << get_num_of_bits_without_msb(-1) << std::endl;                  // int (signed)     -> 31
    std::cout << get_num_of_bits_without_msb(1u) << std::endl;                  // unsigned int     -> 32
    std::cout << get_num_of_bits_without_msb(1ul) << std::endl;                 // unsigned long    -> 64
    std::cout << get_num_of_bits_without_msb(-1l) << std::endl << std::endl;    // long (signed)    -> 63

    std::cout << get_num_of_bits('a') << std::endl;                             // char (signed)    -> 8
    std::cout << get_num_of_bits(-1) << std::endl;                              // int (signed)     -> 32
    std::cout << get_num_of_bits(1u) << std::endl;                              // unsigned int     -> 32
    std::cout << get_num_of_bits(1ul) << std::endl;                             // unsigned long    -> 64
    std::cout << get_num_of_bits(-1l) << std::endl;                             // long (signed)    -> 64

    return 0;
}
