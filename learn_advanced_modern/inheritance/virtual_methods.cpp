#include <iostream>

class Vehicle {
public:
    virtual void accelerate() {}
};

class Car : public Vehicle {
public:
    // method actually virtual (as virtual in the base class)
    void accelerate() override {
        std::cout << "> car is accelerating" << std::endl;
    }
};

class Electric : public Car {
public:
    // method still virtual
    void accelerate() override {
        std::cout << "> electric car is accelerating" << std::endl;
    }
};

int main() {
    Vehicle* vehicle;
    
    vehicle = new Car();    
    vehicle->accelerate();
    delete vehicle;

    vehicle = new Electric();
    vehicle->accelerate();
    delete vehicle;

    Car* car;
    car = new Car();
    car->accelerate();
    delete car;

    car = new Electric;
    car->accelerate();
    delete car;

    return 0;
}
