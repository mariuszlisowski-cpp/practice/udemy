#include <fstream>
#include <iostream>
#include <string>

int main() {
    std::ifstream file_in;
    
    std::string filename{"test"};
    file_in.open(filename);

    if (file_in.is_open()) {
        std::string line;
        while (std::getline(file_in, line)) {
            std::cout << line << std::endl;
        }
        file_in.close();
    } else {
        std::cout << "Error reading a file: " << filename << std::endl;
    }

    return 0;
}
