#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>

/* byte max is 255 (0xff or 0b 1111 1111)
                            bits  4   4     // NIBBLE_BITS = 4
    4796
    0001 0010 1011 1100     // 4796 % HEX_BASE = c
    0000 0001 0010 1011     //  299 % HEX_BASE = b (then >> NIBBLE_BITS)
    0000 0000 0001 0010     //   18 % HEX_BASE = 2 (then >> NIBBLE_BITS)
    0000 0000 0000 0001     //    1 % HEX_BASE = 1 (then >> NIBBLE_BITS)
    
    0000 0000 0000 0000     // last >> NIBBLE_BITS (loop exit)
*/

constexpr unsigned HEX_BASE{ 16 };
constexpr unsigned NIBBLE_BITS{ 4 };

auto decimal_to_hex(unsigned decimal) {
    std::stringstream ss;
    do {
        ss << std::hex << decimal % HEX_BASE;                           // convert single nibble to hex (0-f)
    } while (decimal >>= NIBBLE_BITS);                                  // next nibble
    
    auto hex = ss.str();
    std::reverse(hex.begin(), hex.end());

    return hex;
}

auto decimal_to_hex_short(unsigned decimal) {
    std::stringstream ss;
    ss << std::hex << decimal;
    return ss.str();
}

int main() {
    std::cout << decimal_to_hex(4796) << std::endl;    
    std::cout << decimal_to_hex_short(4796) << std::endl;    

    return 0;
}
