#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>

template <class T, size_t N>
void print_array(std::array<T, N> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
}

int main() {
    // std::array<int, 6> arr{3, 0, 1, 2, 3, 4};
    std::array arr{3, 0, 1, 2, 3, 4}; // C++17 type & size deduction

    int new_value = 0;
    std::replace_if(arr.begin(), arr.end(),
        [](int n) { return n >= 3; },
        new_value);

    print_array(arr);

    return 0;
}
