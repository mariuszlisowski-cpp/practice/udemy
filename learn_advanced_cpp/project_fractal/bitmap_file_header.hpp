#pragma once

#include <cstdint>

#pragma pack(2)

namespace raisin {

struct bitmap_file_header {
    char header[2]{'B', 'M'};
    int32_t file_size;
    int32_t reserved{};
    int32_t data_offset;
};

}
