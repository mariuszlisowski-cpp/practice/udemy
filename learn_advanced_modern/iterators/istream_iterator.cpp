#include <iostream>

int main() {
    std::cout << "Enter an integer\n: ";
    // reading from a console
    std::istream_iterator<int> is_it(std::cin);
    int value = *is_it;

    std::cout << "> value captured: " << value << std::endl;

    return 0;
}
