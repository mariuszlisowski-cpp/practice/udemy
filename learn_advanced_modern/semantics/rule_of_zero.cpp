#include <string>

/*  The rule of zero:
    You are allowed to not write any of the special member functions when creating your class
*/

class person {
  public:
    person(const std::string& name, int age);       // Constructor

  private:
    std::string name;
    int age;
};

int main() { return 0; }
