#include "fractal_creator.hpp"

using namespace raisin;

int main() {
    fractal_creator f_creator(800, 600);

    f_creator.add_range(0.0, rgb(0, 0, 0));
    f_creator.add_range(0.3, rgb(255, 0, 0));
    f_creator.add_range(0.5, rgb(255, 255, 0));
    f_creator.add_range(1.0, rgb(255, 255, 255));

    f_creator.add_zoom(zoom(295, 202, .1));
    f_creator.add_zoom(zoom(312, 304, .1));
    f_creator.run("test.bmp");

    return 0;
}
