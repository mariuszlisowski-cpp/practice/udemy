/*  Rule of Three:
    - destructor
    - copy constructor
    - copy assignment operator
*/
#include <iostream>

class Foo {
};

class Bar {
public:
    Bar() {
        foo_ = new Foo();
        std::cout << "> creating Bar at: " << this << std::endl;
    }
    /* destructor */                                                    // 1
    ~Bar() {
        delete foo_;
        std::cout << "> deleting Bar at: " << this << std::endl;
    }

    /* copy constructor */                                              // 2
    Bar(const Bar& other) {
        foo_ = new Foo();                                               // deep copy (new address acquired)
        std::cout << "> acquiring a new address" << std::endl;
    }

    /* copy assignment operator */                                      // 3
    Bar& operator=(const Bar& other) {
        *foo_ = *other.foo_;                                            // deep copy (reuse of object's memory)
        std::cout << "> reusing en existing address" << std::endl;

        return *this;
    }

private:
    Foo* foo_;
};

int main() {
    Bar original;
    Bar copy{ original };                                               // copy c'tor (deep copy)
    copy = original;                                                    // assignment operator (deep copy)

    return 0;
}                                                                       // both Foo objects destructed
