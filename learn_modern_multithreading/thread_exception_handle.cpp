#include <exception>
#include <iostream>
#include <thread>

int main() {
    std::thread th;
    
    try {
        throw std::exception();         // anything that can throw an exception
    } catch (std::exception& e) {
        th.join();                      // joins before thread's dectructor is called
    }
    
    th.join();                          // regular (no exception called)

    return 0;
}
