#include <iostream>
#include <vector>


int main() {
    std::vector<const char*> cstrings{"hello!", ", ", "World"};

    std::vector<const char*>::reverse_iterator it = cstrings.rbegin();
    while (it != cstrings.rend()) {
        std::cout << *it;
        ++it;
    }

    return 0;
}
