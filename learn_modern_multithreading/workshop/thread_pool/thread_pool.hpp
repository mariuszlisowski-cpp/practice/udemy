#ifndef THREAD_POOL_HPP
#define THREAD_POOL_HPP

#include "concurrent_queue.hpp"
#include <functional>
#include <thread>
#include <vector>

using func = std::function<void()>;                                             // no arguments nor returning

class thread_pool {
public:
    thread_pool() {
        const unsigned thread_count = std::thread::hardware_concurrency();
        for (unsigned i = 0; i < thread_count; ++i) {
            threads.push_back(std::thread{&thread_pool::worker, this});         // address of a member function
                                                                                // plus an argument as non-static
        }
    }

    ~thread_pool() {
        for (auto& t: threads) {                                                // movable only threads thus ref
            t.join();
        }
    }

    void submit(func f) {
        work_queue.push(f);
    }

private:
    concurrent_queue<func> work_queue;
    std::vector<std::thread> threads;

    void worker() {
        while (true) {
            func task;                                                          // callable object
            work_queue.pop(task);                   
            task();
        }
    }
};

#endif
