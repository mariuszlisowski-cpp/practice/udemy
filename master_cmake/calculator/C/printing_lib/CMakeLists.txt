# 1.0 build library target
add_library(printing
    source/print_result.cpp
)

# 2.0 add header files directory (target printing)
target_include_directories(printing PUBLIC include) # PUBLIC as target is using include directory
                                                    # AND include directory is used by other targets
                                                    # (in main which includes headers)
