#include <future>
#include <iostream>
#include <thread>

using namespace std::literals;

void test() {
    std::cout << "> test: going to sleep..." << std::endl;
    std::this_thread::sleep_for(3s);
    std::cout << "> test: woken up" << std::endl;

}

int main() {
    std::cout << "> main: started" << std::endl;
    auto future = std::async(test);	                            // MUST return std::future
																// to be asynchronous
    std::cout << "> main: finished" << std::endl;
    
    return 0;
}
