# 1.0 build library target
add_library(calculations
    source/addition.cpp
    source/division.cpp
)

# 2.0 add header files directory (target calculations)
target_include_directories(calculations PUBLIC include) # PUBLIC as target is using include directory
                                                        # AND include directory is used by other targets
                                                        # (in main which includes headers)
