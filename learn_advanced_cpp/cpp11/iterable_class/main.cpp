#include <iostream>
#include <string>

#include "ring.hpp"

int main() {
    const int BUFFER_SIZE{ 3 };
    ring<std::string> textring(BUFFER_SIZE);

    textring.add("one");
    textring.add("two");
    textring.add("three");                                  // maximum reached
    textring.add("four");                                   // override the first
    textring.add("five");                                   // override the second
    textring.add("six");                                    // override the third
    textring.add("seven");                                  // override the first again

    // c style loop
    for (int i = 0; i < textring.size(); i++) {
        std::cout << textring.get(i) << ' ';                // be aware of 'out of range'
    }
    std::cout << std::endl;

    // cpp98 loop
    using ring_iter = ring<std::string>::iterator;
    for (ring_iter it = textring.begin();                   // implemented: begin() & end() 
         it != textring.end(); ++it)                        // in ring::iterator nested class
                                                            // as well as operator++ and operator!=
    {
        std::cout << *it << ' ';                            // operator* also implemented
    }
    std::cout << std::endl;

    // cpp11 range-based loop
    for (std::string& str : textring) {                     // uses iterators underneath
        std::cout << str << ' ';
    }
    std::cout << std::endl;

    return 0;
}
