#include <cstdio>
#include <cstdint>

struct rgb565 {
    unsigned short b : 5;
    unsigned short g : 6;
    unsigned short r : 5;
};

template <typename T, typename RawVal>
void printStructInfo() {
    printf("-----------------------------\n");
    printf("sizeof %s = %lu\n", __PRETTY_FUNCTION__, sizeof(T));
    
    T val;
    val.r = 15;
    val.g = 63;
    val.b = 31;

    printf("packed struct contains: %d, %d, %d\n", val.r, val.g, val.b);

    RawVal rawVal = *(RawVal*)&val;
    printf("raw value is 0x%x\n", rawVal);
}

int main() {
    printStructInfo<rgb565, unsigned short>();

    uint16_t color = 0x7fff;
}
