#include <bitset>
#include <iostream>

int main() {
    {
    /* two's complement
       char [-128, 0] 128 values
            [0,  127] 128 values
            total:    256 values
    */
    char positive = 127;                                    // max of positive 8bit
    std::bitset<8> binary_positive(positive);
    std::cout << (int)(positive) << "\t\t"
              << binary_positive << std::endl;
    
    char negative = -128;                                   // min of negative 8bit
    std::bitset<8> binary_negative(negative);
    std::cout << (int)(negative) << '\t'
              << binary_negative << std::endl;
    }

    {
    char positive = 7;
    std::bitset<8> binary_positive(positive);
    std::cout << (int)(positive) << "\t\t"
              << binary_positive << std::endl;
    
    char negative = -8;
    std::bitset<8> binary_negative(negative);
    std::cout << (int)(negative) << "\t\t"
              << binary_negative << std::endl;
    }

    return 0;
}
