#include <iostream>
#include <iterator>
#include <vector>

int main() {
    std::vector<int> vec{1, 2, 3};

    // insert into any given position
    std::vector<int>::iterator c_it = vec.begin();
    ++c_it;     // move to a second position
    std::insert_iterator<std::vector<int>> i_it = std::inserter(vec, c_it);
    *i_it = 9;  // insert

    // insert at the begiging
    auto ii_it = std::inserter(vec, vec.begin());
    *ii_it = 0;
    // or
    *vec.begin() = -1;

    for (const auto value : vec) {
        std::cout << value << ' ';
    }
    
    return 0;
}
