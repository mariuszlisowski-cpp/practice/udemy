#include <iostream>

int main() {
    int value = 5;

    // has to be const (is bound to temporary NOT to value)
    const float& f = value; // value is of type int and is being converted to float (so value temporary is created)
    
    std::cout << "> I'm const float: " << f << std::endl;

    return 0;
}
