#include <iostream>
#include <map>
#include <string>

class Person {
public:
    Person() = default;
    // non-default copy constructor (deep copy)
    Person(const Person& other) : name_(other.name_), age_(other.age_) {
        std::cout << "> copy constructor" << std::endl;
    }
    Person(std::string name, int age) :
        name_(name), age_(age) {}

    void print() const {
        std::cout << name_ << ": " << age_ << std::endl;
    }

private:
    std::string name_;
    int age_;
};

int main() {
    std::map<int, Person> people;

    people[0] = Person("Mike", 33);
    people[1] = Person("Elen", 29);
    people[2] = Person("Mary", 41);

    people.insert(std::make_pair(4, Person("Gary", 19)));                           // copy constructor invoke

    for (auto it{ people.begin() }; it != people.end(); ++it) {
        it->second.print();
    }

    return 0;
}
