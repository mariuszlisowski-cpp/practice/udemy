/*  perfect forwarding
    ~ the following properties of the forwarded arguments are preserved:
      - modifable
      - unmodifable
      - moveable
*/

#include <iostream>
#include <iterator>

class X {};

// better to use a template speclialization (!)
void g(X& x) { std::cout << "> modiflable" << std::endl; }
void g(const X& x) { std::cout << "> unmodiflable" << std::endl; }
void g(X&& x) { std::cout << "> moveable" << std::endl; }

// calls modifable
void f(X& x) { g(x); }
// calls unmodifable
void f(const X& x) { g(x); }
// calls moveable
void f(X&& x) { g(std::move(x)); }

int main() { 
    X x;
    f(x);               // f(int&) calls g(int&)

    const X xx;
    f(xx);              // f(const int&) calls g(const int&)

    f(X());             // f(int&&) calls g(int&&)
    

    return 0;
}
