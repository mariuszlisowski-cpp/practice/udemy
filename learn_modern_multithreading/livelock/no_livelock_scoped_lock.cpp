#include <thread>
#include <mutex>
#include <iostream>
#include <string>

using namespace std::literals;

std::mutex mutex1, mutex2;

void cecil() {
	std::this_thread::sleep_for(10ms);
    
    std::cout << "After you, Claude!" << std::endl;
    std::scoped_lock lk(mutex1, mutex2);                                // lock both mutexes
    
    std::this_thread::sleep_for(1s);	    
    std::cout << "> thread 1 has locked both mutexes" << std::endl;
}

void claude() {
    std::cout << "No, after you, Cecil!" << std::endl;
    std::scoped_lock lk(mutex1, mutex2);                                // lock both mutexes
    
    std::this_thread::sleep_for(1s);	    
    std::cout << "> thread 2 has locked both mutexes" << std::endl;
}

int main() {
	std::thread t1(cecil);
	std::this_thread::sleep_for(10ms);
	std::thread t2(claude);

	t1.join();
	t2.join();
}
