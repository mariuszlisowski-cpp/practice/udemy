#include <iostream>

int main() {

    int number;
    while (true) {
        std::cout << "Enter a number: ";
        std::cin >> number;
        
        if (std::cin.good()) {
            std::cout << "> You entered: " << number << std::endl;
        } else if (std::cin.fail()) {
            std::cout << "> Wrong! Enter again..." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        } else if (std::cin.bad()) {
            std::cout << "> Something bad happened!" << std::endl;
        }
    }

    return 0;
}
