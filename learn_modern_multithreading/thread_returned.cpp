#include <chrono>
#include <iostream>
#include <string>
#include <thread>

std::thread f() {
    std::thread th([]{
        std::cout << "Returnig a running thread..." << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    });

    return th;                          // returning rvalue automatically
}

int main() {

    f().join();
    std::cout << "Done!" << std::endl;

    return 0;
}
