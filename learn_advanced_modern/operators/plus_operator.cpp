#include <iostream>

class Clazz {
public:
    Clazz() {}
    Clazz(int value) : value_(value) {}
    // same as compiler generated copy constructor (would be good enough here)
    Clazz(const Clazz& other) : value_(other.value_) {
        std::cout << "> copy constructor used" << std::endl;
    }    

    // same as compiler generated assignment operator (would be good enough here)
    Clazz& operator=(const Clazz& other) {
        // if self-assignment do nothig
        if (this != &other) {
            value_ = other.value_;
            std::cout << "> assignment operator used" << std::endl;
        }

        // this is a pointer to the object the method was call on
        return *this; // reference returned
    }

    // member function (because updates the class)
    Clazz operator+=(const Clazz& rhs) {
        this->value_ += rhs.value_; // lhs object gets modified
        std::cout << "> operator += used" << std::endl;
        
        // after the object was modified it gets dereferenced
        // and returns modified value of the object
        return *this; // value returned
    }

    int getValue() {
        return value_;
    }
private:
    int value_;
};

// non-member function (does not update a class)
Clazz operator+(const Clazz& lhs, const Clazz& rhs) {
    Clazz temp{lhs};    // copy constructor used
    temp += rhs;        // code reuse (class member funtion operator+= used)

    return temp;
}

int main() {
    Clazz c1(8);
    Clazz c2(2);
    Clazz c3;
    
    // member function operator+= called
    c1 += c2;       // c1.operator+=(c2) (c1 passed as this, c2 as reference)
    std::cout << c1.getValue() << std::endl;

    c3 = c1 + c2;   // operator+(c1, c2)
    std::cout << c3.getValue() << std::endl;

    return 0;
}
