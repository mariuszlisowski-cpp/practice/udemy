#include <algorithm>
#include <iostream>
#include <string>

int main() {
    char char_to_discard{'!'};
    std::string str{"print without this sign: ! done!"};

    std::string::iterator it = std::find(str.begin(), str.end(), char_to_discard);
    if (it != str.end()) {
        // algorithm
        std::copy_if(str.begin(), str.end(),
                     std::ostream_iterator<char>(std::cout),
                     [](char c){ return c != '!'; });
    }

    return 0;
}
