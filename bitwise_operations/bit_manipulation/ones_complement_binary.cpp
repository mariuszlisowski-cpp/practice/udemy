#include <bitset>
#include <iostream>

int main() {
    auto binary = 0b1010;                                               // signed
    std::bitset<4> outputA(binary);
    std::cout << outputA << std::endl;

    auto ones_complement = ~binary;
    std::bitset<4> outputB(ones_complement);
    std::cout << outputB << std::endl;

    return 0;
}
