#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

int main() {
    std::vector<std::string> words{"a", "collection", "of", "words", "with", "varying", "lengths"};

    // max lexicographically
    std::vector<std::string>::const_iterator min_lexicographically = 
        std::max_element(words.begin(), words.end());
    std::cout << *min_lexicographically << std::endl;

    // max length (custom comparator)
    auto max_length_word = std::max_element(words.begin(), words.end(),
                               [](const std::string& lhs, const std::string& rhs) {
                                   return lhs.size() < rhs.size();
                               });
    std::cout << "> longest word is: " << *max_length_word << std::endl;    // iterator as above

    return 0;
}
