#include <fstream>
#include <iostream>
#include <string>

int main() {
    std::ofstream file_out;                                         // write only
    
    std::string filename{"test"};
    file_out.open(filename);
    if (file_out.is_open()) {
        file_out << "first line written to a file" << std::endl;    // std::endl flushes stream
        file_out << "second line written to a file" << std::endl;   // instead of std::flush(file_out)
        file_out.close();
    } else {
        std::cout << "Error creating a file: " << filename << std::endl;
    }

    return 0;
}
