#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>

int main() {
    std::string str{"only uppercase, only lowercase"};

    // using an algorithm (half range loop)
    auto it = std::find(str.begin(), str.end(), ',');
    std::for_each(str.begin(), it, [](char& c) {
        c = std::toupper(c);
    });
    std::cout << str << std::endl;

    // using a range loop (full range loop then broken)
    for (auto& c : str) {
        c = std::toupper(c);
        if (c == ',') {
            break;
        }
    }
    std::cout << str << std::endl;

    return 0;
}
