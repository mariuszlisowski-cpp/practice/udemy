#include <iostream>

auto flip_sign(int value) {
    return ~value + 1;    
}

int main() {
    std::cout << flip_sign(255) << std::endl;

    return 0;
}
