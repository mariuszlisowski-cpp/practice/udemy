#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>

int main() {
    std::vector<std::string> numsA{"ala", "ma", "kota"};
    std::vector<std::string> numsB{"ala", "ma", "KOTA"};

    bool are_equal = std::equal(numsA.begin(), numsA.end(),
                                numsB.begin(),
                                [](const std::string& lstr, const std::string& rstr) {
                                    return boost::to_upper_copy(lstr) == boost::to_upper_copy(rstr); // ignore case
                                });

    std::cout << "> containers are " << (are_equal ? "equal" : "NOT equal") << std::endl;
    
    return 0;
}
