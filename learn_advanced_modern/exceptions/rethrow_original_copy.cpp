#include <iostream>
#include <stdexcept>

void throwing() {
    throw std::invalid_argument("ERROR!");
}

void throwing_original() {
    try {
        throwing();
    } catch (const std::invalid_argument& e) {
        std::cerr << e.what() << std::endl;
        throw;                                              // original (as using reference)
    }
}

void throwing_copy() {
    try {
        throwing();
    } catch (const std::invalid_argument& e) {
        std::cerr << e.what() << std::endl;
        throw e;                                            // copy
    }
}

int main() {
    try {
        throwing_original();
    } catch (const std::invalid_argument& e) {
        std::cout << "> exception rethrown" << std::endl;
    }

    try {
        throwing_copy();
    } catch (const std::invalid_argument& e) {
        std::cout << "> exception copy rethrown" << std::endl;
    }

    return 0;
}
