#include <iostream>
#include <sstream>
#include <string>

int main() {
    std::string sentence = "Let me split this into words";

    std::istringstream iss{sentence};
    std::cout << iss.str() << std::endl;

    std::string word;
    while (iss >> word) {
        std::cout << word << std::endl;
    }

    // empty the steam
    iss.str(std::string());
    std::cout << iss.str() << std::endl;

    return 0;
}
