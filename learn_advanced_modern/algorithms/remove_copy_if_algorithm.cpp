#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <class T>
void print_vector(std::vector<T> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> nums{9, 1, 2, 3, 4, 9};
    std::vector<int> out;

    // output
    std::cout << "> size: " << nums.size() << std::endl;
    print_vector(nums);

    // removing elements using predicate (to the end)
    auto predicate = [](int n){ return n > 4; };
    std::remove_copy_if(nums.begin(), nums.end(), std::back_inserter(out), predicate);
    
    // output
    std::cout << "\n> size: " << out.size() << std::endl;      // proper size now
    print_vector(out);

    return 0;
}
