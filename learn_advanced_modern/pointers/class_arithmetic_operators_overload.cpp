#include <iomanip>
#include <iostream>
#include <ostream>

class Complex {
public:
	Complex(double real, double imaginery)
		: real_(real), imaginary_(imaginery) {}

	Complex& operator+(const Complex& rhs) {
		real_ += rhs.real_;
		imaginary_ += rhs.imaginary_;

		return *this;
	}

	// prefix (++this)
	Complex& operator++() {
		++real_;
		++imaginary_;

		return *this;				// returns a reference
	}
	// postfix (this++)
	Complex operator++(int dummy) { // function prototype must be different thus dummy argument
		Complex temp(*this);

		++real_;
		++imaginary_;

		return temp; 				// returns a value
	}

	// prefix (--this)
	Complex& operator--() {
		--real_;
		--imaginary_;

		return *this;				// returns a reference
	}
	
	// postfix (this--)
	Complex operator--(int dummy) { // function protorype must be different thus dummy argument
		Complex temp(*this);
		--real_;
		--imaginary_;

		return temp; 				// returns a value
	}

	friend std::ostream& operator<<(std::ostream& os, const Complex& obj);	

private:
	double real_;
	double imaginary_;
};

std::ostream& operator<<(std::ostream& os, const Complex& obj) {
	os << obj.real_ << " + " << obj.imaginary_ << 'i';
	return os;
}

int main() {
	Complex complexA{3.4, 2.7};
	Complex complexB{1.6, 3.3};

	std::cout << std::setw(25) << std::left
	  		  << "> complex: " << complexA << std::endl
			  << std::setw(25) << std::left
			  << "> incremented (pre): " << ++complexA << std::endl
			  << std::setw(25) << std::left
			  << "> complex: " << complexB << std::endl
			  << std::setw(25) << std::left
			  << "> decremented (pre): " << --complexB << std::endl
			  << std::setw(25) << std::left
	          << "> summed up: " << complexA + complexB << std::endl
			  << std::setw(25) << std::left
			  << "> incremented (post): " << complexA++ << std::endl
			  << std::setw(25) << std::left
			  << "> complex: " << complexA << std::endl
			  << std::setw(25) << std::left
			  << "> decremented (post): " << complexA-- << std::endl
			  << std::setw(25) << std::left
			  << "> complex: " << complexA << std::endl;

	return 0;
}
