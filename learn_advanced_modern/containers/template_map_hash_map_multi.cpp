#include <iostream>
#include <map>
#include <unordered_map>
#include <string>

template<typename Map>
void print_map(const Map& map) {
    for (const auto& pair : map) {
        std::cout << pair.first  << " : " << pair.second << std::endl;
    }
    std::cout << std::endl;
}

int main() {
    // hash map
    std::unordered_multimap<std::string, int> multi_scores {
        {"tom", 1}, {"ola", 2}, {"ela", 3}, {"ola", 4},
    };
    print_map(multi_scores); // not sorged (organized into buckets)

    // hash map
    std::unordered_map<std::string, int> unique_scores {
        {"tom", 1}, {"ola", 2}, {"ela", 3}, {"ola", 4},
    };
    print_map(unique_scores); // not sorted (organized into buckets)

    std::multimap<std::string, int> sorted_multi_scores {
        {"tom", 1}, {"ola", 2}, {"ela", 3}, {"ola", 4},
    };
    print_map(sorted_multi_scores); // sorted by name

    std::map<std::string, int> sorted_unique_scores {
        {"tom", 1}, {"ola", 2}, {"ela", 3}, {"ola", 4},
    };
    print_map(sorted_unique_scores); // sorted by name

    return 0;
}
