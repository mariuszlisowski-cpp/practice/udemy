#ifndef CONCURRENT_QUEUE_HPP
#define CONCURRENT_QUEUE_HPP

#include <exception>
#include <iostream>
#include <queue>
#include <mutex>
#include <string>

class concurrent_queue_exception : public std::exception {
public:
    concurrent_queue_exception(const std::string& message) :
        message_(message) {}
    
    const char* what() const noexcept override {
        return message_.c_str();
    }
    
private:
    std::string message_;
};

template <typename T>
class concurrent_queue {
public:
    concurrent_queue() = default;
    ~concurrent_queue() = default; 
    concurrent_queue(const concurrent_queue&) = default;                // copy c'ctor
    concurrent_queue(concurrent_queue&&) = default;                     // move c'ctor
    concurrent_queue& operator=(const concurrent_queue&) = default;     // copy assignment
    concurrent_queue& operator=(concurrent_queue&&) = default;          // move assignment

    void push(const T& value) {
        std::lock_guard<std::mutex> lg(m_);
        q_.push(value);
    }

    T pop() {
        std::unique_lock<std::mutex> ul(m_);
        if (q_.empty()) {
            throw concurrent_queue_exception("The queue is empty!");
        }
        T value = q_.front();
        q_.pop();

        return value;
    }

private:
    std::queue<T> q_;
    std::mutex m_;
};

#endif
