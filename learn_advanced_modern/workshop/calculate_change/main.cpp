#include "coin.hpp"

#include <fstream>
#include <iostream>
#include <vector>

int check_input() {
	int input{0};
	do {
		std::cout << "Please enter the amount of change (between 1 and 499 pennies) or 0 to exit: ";
		std::cin >> input;
	} while (input < 0 || input > 499);

	return input;
}

std::vector<Coin> populate_coins() {
	std::string filename{"coins_us.txt"};
	std::vector<Coin> coins;
	std::ifstream file(filename);

	if (file.is_open()) {
		std::string name;
		std::string plural_name;
		int cents;
		while (file >> name >> plural_name >> cents) {
			coins.push_back(Coin{ name, plural_name, cents });
		}
		file.close();
	} else {
		std::cout << "File open error!" << std::endl;
	}

	return coins;
}

int main() {
    std::vector<Coin> coins;
    coins = populate_coins();

    int input{1};
    while (input != 0) {
        input = check_input(); // must be between 1 - 499 cents

        int change{input};
        for (auto coin : coins) {
            change = coin.do_coin(change);
        }
    }

    return 0;
}
