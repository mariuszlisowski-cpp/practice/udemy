#include <cstddef>
#include <iostream>

class Clazz {
public:
    Clazz(size_t size) : size_(size) {}
    
    void caputre_object_inside() {
        // *this object captured by reference
        // as 'this' is a pointer to an object (although [&this] cannot bu use)
        auto functor_with_caputred_object =
            [this]() {
                std::cout << "> caputred object's value: " 
                          << this->size_ << std::endl;
                ++this->size_;  // caputred by reference thus can be modified
                std::cout << "> midified object's value: " 
                          << this->size_ << std::endl;

            };
        functor_with_caputred_object();
    }

private:
    size_t size_;
};

int main() {
    Clazz object(4);

    object.caputre_object_inside();


    return 0;
}
