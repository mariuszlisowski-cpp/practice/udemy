#include <iostream>
#include <vector>

template<typename T>
void print_size(const std::vector<T>& v) {
    std::cout << "> size: " << v.size() << ", ";
}

template<typename T>
void print_capacity(const std::vector<T>& v) {
    std::cout << "> capacity: " <<v.capacity() << std::endl;
}

int main() {
    /* manual */
    std::vector<int> v;                                         // an empty vector
    v.reserve(100);                                             // prevent resizing while incrementing up to arg
    v.push_back(9);                                             // elements should be added up to argument

    print_size(v);                                              // added elements
    print_capacity(v);                                          // allocated space

    /* constructor initialization */
    std::vector<int> filled(100);                               // size of an argument passed (zeroes)
    std::cout << filled[50] << std::endl;                       // safe access as already initialized
    filled.push_back(7);                                        // resizes a vector as a new added
    
    print_size(v);                                              // added elements
    print_capacity(v);                                          // increased capacity (x2)

    /* pointer to a vector */
    int* ptr{ v.data() };                                       // do NOT delete this pointer
                                                                // as vector's memory will be deleted
    std::cout << (ptr ? *ptr : ' ') << std::endl;

    return 0;
}
