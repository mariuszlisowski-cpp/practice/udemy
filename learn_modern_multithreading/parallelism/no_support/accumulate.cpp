#include <iostream>
#include <numeric>
#include <vector>

int main() {
	std::vector<int> v {1, 2, 3, 4, 5, 6, 7, 8};

	// no support for execution policies
	auto sum = std::accumulate(v.begin(), v.end(), 0);          // supportive eqivalent is std::recude
	
	std::cout << "Vector elements: ";
	for (auto i : v) {
		std::cout << i << ", ";
	}
	std::cout << "\nSum of elements is " << sum << std::endl;
}
