#include <iostream>

// abstract class
class Vehicle {
public:
    // a must if class is abstract
    virtual ~Vehicle() {}

    virtual void accelerate() = 0; // pure virual method
    virtual void stop() {}
};

class Car : public Vehicle {
public:
    // method actually overrides the base class (check passed)
    void accelerate() override {
        std::cout << "> car is accelerating" << std::endl;
    }

    /* overloaded method - does not overrides
    void accelerate(int rate) override {} */

    void stop() override final {} // no further overriding
};

class Electric : public Car {
public:
    void accelerate() override {
        std::cout << "> electric car is accelerating" << std::endl;
    }

    /* final method cannot be overridden
    void stop() {}; */
};

int main() {
    Vehicle* vehicle;
    
    vehicle = new Car();    
    vehicle->accelerate();
    delete vehicle;

    vehicle = new Electric();
    vehicle->accelerate();
    delete vehicle;

    Car* car;
    car = new Car();
    car->accelerate();
    delete car;

    car = new Electric;
    car->accelerate();
    delete car;

    return 0;
}
