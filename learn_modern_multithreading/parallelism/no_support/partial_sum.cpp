#include <numeric>
#include <iostream>
#include <vector>

int main() {
	std::vector<int> v {1, 2, 3, 4, 5, 6, 7, 8};
	std::vector<int> v2;

	// no support for execution policies
	std::partial_sum(v.begin(), v.end(),  back_inserter(v2));       // supportive eqivalents are
                                                                    // std::inclusive_scan & std::exclusive_scan
	std::cout << "Original vector elements: ";
	for (auto i : v) {
		std::cout << i << ", ";
	}
	
	std::cout << "\nPartial sum vector elements: ";
	for (auto i : v2) {
		std::cout << i << ", ";
	}
}
