#include <atomic>
#include <mutex>
#include <iostream>
#include <vector>
#include <thread>

class some_type {
public:
	void do_it() {
		std::cout << ">done" << std::endl;
	}
};

std::atomic<some_type*> ptr{nullptr};                     // Variable to be lazily initialized
std::mutex process_mutex;

void process() {
    if (!ptr) {                                           // first check of ptr
        std::lock_guard<std::mutex> lk(process_mutex);
        
        if (!ptr) {						                  // second check of ptr
            ptr = new some_type;						  // atomically initialized pointer
														  // no need to use atomic since c++17
														  // operations order of object creation is guaranteed
		}
    }
	some_type* sp = ptr;								  // atomic pointer cannot be dereferenced thus copied
	sp->do_it();										  // pointer used (non-atomic)
}

int main() {
	std::vector<std::thread> threads;
	
	for (int i = 0; i < 10; ++i) {
		threads.push_back(std::thread{process});
	}

	for (auto&t : threads) {
		t.join();
	}
}
