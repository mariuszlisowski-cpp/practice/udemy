#include <algorithm>
#include <cstring> // strlen()
#include <iostream>
#include <string>
#include <utility> // std::pair
#include <vector>

int main() {
    // custom predicate (comparator)
    std::pair<const char *, const char *> minmax_pair =
        std::minmax({"collection", "of", "words", "only"},
                    [](const char *lhs, const char *rhs) {
                        return std::strlen(lhs) < std::strlen(rhs);
                    });

    std::cout << "> shortest word: " << minmax_pair.first << std::endl;
    std::cout << "> longest word:  " << minmax_pair.second << std::endl;

    return 0;
}
