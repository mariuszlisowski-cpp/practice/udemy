#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

const int STRING_LENGTH { 3 };

bool match(std::string str) {
    return str.size() == STRING_LENGTH;                                     // any criteria
}

int countStrings(std::vector<std::string>& nums,
                 bool (*match)(std::string str))                            // argument as a function pointer
{
    int count{};
    for (auto& num : nums) {
        if (match(num)) {                                                   // using a function
            ++count;
            std::cout << num << std::endl;
        }
    }

    return count;
}

int main() {
    std::vector<std::string> numbers {
        "one", "two", "three", "four", "five", "six"
    };

    auto result { std::count_if(numbers.begin(), numbers.end(), match) };   // function as a predicate
    std::cout << result << std::endl;

    auto count { countStrings(numbers, match) };                            // passing a function
    std::cout << count << std::endl;

    return 0;
}
