#include <iostream>

auto decrement(int value) {
    return ~-value;
}

auto increment(int value) {
    return -~value;
}

int main() {
    int number{ 42 };
	
	std::cout << decrement(number) << std::endl;
	std::cout << increment(number) << std::endl;

    return 0;
}
