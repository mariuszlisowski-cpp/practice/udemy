#include <iostream>

// functor (class with an overloaded function call operator)
class Even {
public:
    // any number and type of arguments, return any type or nothing
    bool operator()(int value) {
        return !(value % 2);
    }
};

int main() {
    Even even;

    // invoking operator()
    bool is_even = even(2); // class instance behaves like a callable function

    std::cout << std::boolalpha << is_even << std::endl;

    return 0;
}
