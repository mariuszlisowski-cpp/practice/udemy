#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

int main() {
    std::ifstream ifs;
    ifs.open("numbers");

    std::ostringstream oss;
    if (ifs.is_open()) {
        std::string line;
        while (std::getline(ifs, line)) {
            std::istringstream iss(line);
            int number;
            // reads until a whitespace (discards LF)
            while (iss >> number) {
                oss << number << ' ';
            }
        }
        ifs.close();
    } else {
        std::cout << "File open error!" << std::endl;
        exit(1);
    }
    std::cout << oss.str() << std::endl;

    return 0;

}
