#include <algorithm>
#include <iostream>
#include <iterator> // std::back_inserter()
#include <vector>

template <typename T> void print_vector(const std::vector<T> vec) {
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(std::cout, " "));
}

int main() {
    std::vector<int> inputA{1, 2, 3, 4, 5};
    std::vector<int> inputB{2, 3, 4, 5, 6}; // should have same size
    std::vector<int> output;

    // binary operation
    std::transform(inputA.begin(), inputA.end(), inputB.begin(),
                   std::back_inserter(output),
                   [](int inA, int inB) { return inA + inB; });
    print_vector(output);

    return 0;
}
