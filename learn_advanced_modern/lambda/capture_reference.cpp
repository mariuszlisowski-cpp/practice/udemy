#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

int main() {
    std::vector<std::string> words{"a", "collection" , "of", "words", "with", "varying", "lengths"};
    const size_t LENGTH_LIMIT = 4; // no need to capture by lambda as const

    size_t index{};
    auto it = std::find_if(words.begin(), words.end(),
                          [&index](const std::string& str) mutable {
                              ++index;
                              return str.length() > LENGTH_LIMIT;
                          }); // local variable captured by reference and modified

    std::cout << "> first longer than " << LENGTH_LIMIT << " letters: " << *it << std::endl;
    std::cout << "> index of this word: " << index << std::endl; // calculated in lambda

    return 0;
}
