#include <iostream>
#include <iterator>
#include <string>

class Point {
public:
    Point() : x_(0), y_(0) {}
    Point(const Point& other) : x_(other.x_), y_(other.x_) {}

    // combined assignment operator (copy & move)
    Point& operator=(Point other) {                                         // called by Clazz assignment operator
        x_ = other.x_;
        y_ = other.y_;
        std::cout << "> point assignment operator" << std::endl;
        return *this;
    }

private:
    int x_;
    int y_;
};

class Clazz {
public:
    Clazz() : value_(0) {}
    Clazz(int value) : value_(value) {}

    // combined assignment operator (copy & move)
    Clazz& operator=(Clazz other) noexcept {                                // argument passed by value
        value_ = other.value_;                                              // regular assign (or swap)
        point_ = std::move(other.point_);                                   // move assignment of Point called
        std::cout << "> clazz assignment operator" << std::endl;

        return *this;
    }

    int getValue() {
        return value_;
    }

private:
    int value_;
    Point point_;
};

int main() {
    Clazz clazzA;
    Clazz clazzB;

    clazzB = clazzA;                // combined assignment
    clazzB = Clazz();               // combined assignment (pointless?)

    return 0;
}
