#include <iomanip>
#include <iostream>
#include <iterator>
#include <list>
#include <string>

template<class T>
void print_container(const T& container, std::string message) {
    std::cout << std::setw(25) << std::left << message;
    for (auto el : container) { std::cout << el << ' '; }
    std::cout << std::endl;
}

int main() {
    std::list<int> lst{2, 3, 4}; // doubly linked list (constists of nodes with prev & next pointers)

    lst.push_front(1);
    lst.push_back(5);
    print_container(lst, "> pushed front & back:");

    lst.pop_front();
    lst.pop_back();
    print_container(lst, "> popped front & back:");

    lst.reverse();
    print_container(lst, "> reversed:");

    lst.sort(); // no random access to elements thus internal implementation (instead of std::sort())
    print_container(lst, "> sorted:");
    
    auto it = std::next(lst.end(), -2); // iterator increment & decrement
    lst.insert(it, 9);
    print_container(lst, "> inserted:");
    
    lst.erase(it); // FAST
    print_container(lst, "> erased:");

    lst.remove(9); // FAST, no need to erase after (all nodes with such a value deleted)
    print_container(lst, "> removed:");

    std::list<int> lst_1st{1, 3};           // sorted thus 'this' list will be also sorted
    lst_1st.merge(lst);
    print_container(lst_1st, "> merged:");
    print_container(lst, "> original: ");   // has been moved to 'this' list thus empty now

    std::list<int> lst_2nd{9, 3, 0};        // not sorted thus 'this' list will not be sorted either
    lst_2nd.merge(lst_1st);
    print_container(lst_2nd, "> merged:");

    lst_2nd.splice(++lst_2nd.cbegin(), std::list<int>{0, 0}); // start form 2nd iterator
    print_container(lst_2nd, "> spliced:");

    return 0;
}
