// #include <exception>
#include <iostream>
#include <stdexcept>
#include <string>

class bad_student_grade : public std::out_of_range {
public:
    // default
    bad_student_grade() : std::out_of_range("Invalid grade!") {}
    // cstring
    bad_student_grade(const char* message) : std::out_of_range(message) {}
    // string
    bad_student_grade(const std::string& message) : std::out_of_range(message) {}
    
    /* no need for a copy constructor as no data members (compiler generated is enough)
    bad_student_grade(const bad_student_grade& other) : std::out_of_range(other) {} */

    /* desctuctor no needed as nothing allocated (compiler generated is enough)
    ~bad_studend_grade() {} */

     // not actually needed (as a parent class method would be used)
    const char* what() const noexcept override {
        return std::out_of_range::what();
    }
};
 
void throw_custom_exception() {
    throw bad_student_grade();
}

int main() {
    try {
        throw_custom_exception();
    } catch (const bad_student_grade& e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}
