#include <string>

class Coin {
public:
    Coin(const std::string& name, const std::string& plural, int cents);
    int do_coin(int change) const;

private:
    std::string name_;
    std::string plural_name;
    int cents_;
};
