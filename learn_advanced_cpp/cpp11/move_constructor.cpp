#include <iostream>
#include <vector>

class Test {
public:
    Test() {
        pBuffer_ = new int[SIZE]{};                                         // zeroed array
    }
    Test(int val) {
        pBuffer_ = new int[SIZE]{};                                         // code repetition
                                                                            // default c'tor should be called
        for (int i = 0; i < SIZE; i++) {
            pBuffer_[i] = val;
        }
    }
    Test(const Test& other) {
        std::cout << "copy constructor" << std::endl;
        // pBuffer_ = new int[SIZE]{};                                      // code repetition
                                                                            // default c'tor should be called
        Test();

        memcpy(pBuffer_, other.pBuffer_, sizeof(*pBuffer_) * SIZE);         // dereference then multiply
    }
    /* move constructor */
    Test(Test&& other) {
        std::cout << "move constructor" << std::endl;
        pBuffer_ = other.pBuffer_;
        other.pBuffer_ = nullptr;
    }

    Test& operator=(const Test& other) {
        std::cout << "assignment operator" << std::endl;
        // pBuffer_ = new int[SIZE]{};                                      // code repetition
        // memcpy(pBuffer_, other.pBuffer_, sizeof(*pBuffer_) * SIZE);      // copy c'tor should be called instead
        Test temp(other);
        std::swap(temp, *this);

        return *this;
    }

    ~Test() {
        delete[] pBuffer_;
    }

    friend std::ostream& operator<<(std::ostream& out, const Test& test);

private:
    static const int SIZE{ 100 };
    int* pBuffer_{};                                                        // nullptr initialization
};

std::ostream& operator<<(std::ostream& out, const Test& test) {
    out << "Hello from test";
    return out;
}

int main() {
    std::vector<Test> vec;
    vec.push_back(Test());                                                  // move c'tor called instead of copy c'tor
                                                                            // as it is explicitly declared
                                                                            // copy elision at work
    


    return 0;
}
