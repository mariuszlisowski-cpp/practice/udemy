#include <iostream>
#include <stack>

class Test {
public:
    Test() = default;                                                       // good practice
    Test(const std::string& name) : name_(name) {}

    void print() const {                                                    // does not modify an object
        std::cout << name_ << std::endl;
    }

    friend std::ostream& operator<<(std::ostream&, const Test&);

private:
    std::string name_;
};

std::ostream& operator<<(std::ostream& os, const Test& t) {
    os << t.name_ << ' ';

    return os;
}

template<typename T>
void print_stack(std::stack<T> s) {
    std::cout << "# stack: ";
    while (!s.empty()) {
        std::cout << s.top() << ' ';
        s.pop();
    }
    std::cout << std::endl;
}

int main() {
    std::stack<Test> test;                                                  // LIFO data structure
    test.push(Test("aa"));
    test.push(Test("bb"));
    test.push(Test("cc"));
    test.push(Test("dd"));
    print_stack(test);

    Test& top = test.top();                                                 // read/write reference
    std::cout << "> last: ";
    top.print();
    
    top = Test("zz");                                                       // replacing top of the stack
    std::cout << "> last changed: ";
    top.print();
    print_stack(test);

    test.pop();                                                             // removing top of the stack
    
    top = test.top();
    std::cout << "> last after popping: ";
    top.print();

    print_stack(test);

    return 0;
}
