#include <cstddef>
#include <iostream>
#include <memory>

int main() {
    // recommended (from c++11)
    std::shared_ptr<int> sptrA{std::make_shared<int>(77)}; // counter 1
    
    // the memory object and reference counter are allocated separately
    std::shared_ptr<int> sptrB(new int(99)); // less efficient 

    // copy constructor
    std::shared_ptr<int> sptrC(sptrA);       // counter increased (counter 2)
    
    // assignment operator
    std::shared_ptr<int> sptrD;              // counter increased (counter 3)
    sptrD = sptrA;

    std::cout << "> count A: " << sptrA.use_count() << std::endl; // point...
    std::cout << "> count C: " << sptrC.use_count() << std::endl; // same...
    std::cout << "> count D: " << sptrD.use_count() << std::endl; // object
    std::cout << std::endl;
    
    std::cout << "> count B: " << sptrB.use_count() << std::endl; // not shared so far
    std::cout << std::endl;

    sptrD = sptrB; // decrement A & C, increment B (D points to B now)
    std::cout << "> count B: " << sptrB.use_count() << std::endl; // 1st pointer
    std::cout << "> count D: " << sptrD.use_count() << std::endl; // 2nd pointer
    std::cout << "> count A: " << sptrA.use_count() << std::endl; // decremented (no D anymore pointing)
    std::cout << "> count C: " << sptrC.use_count() << std::endl; // decremented

    return 0;
}
