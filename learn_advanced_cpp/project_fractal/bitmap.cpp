#include <fstream>

#include "bitmap.hpp"
#include "bitmap_file_header.hpp"
#include "bitmap_info_header.hpp"

namespace raisin {
    
bitmap::bitmap(int width, int height)
    : width_(width)
    , height_(height)
    , pixels_(new uint8_t[width*height*3]{}) {}
bitmap::~bitmap() {}

bool bitmap::write(const std::string& filename) {
    bitmap_file_header file_header;
    bitmap_info_header info_header;
    
    file_header.file_size = static_cast<int32_t>(sizeof(bitmap_file_header)) + 
                            static_cast<int32_t>(sizeof(bitmap_info_header)) + 
                            (width_ * height_ * 3);
    file_header.data_offset = sizeof(bitmap_file_header) + sizeof(bitmap_info_header);

    info_header.width = width_;
    info_header.height = height_;

    std::ofstream file_out;
    file_out.open(filename, std::ios::binary);
    if (!file_out) {
        return false;
    }
    file_out.write((char*)&file_header, sizeof(file_header));
    file_out.write((char*)&info_header, sizeof(info_header));
    file_out.write((char*)pixels_.get(), width_ * height_ * 3);

    file_out.close();

    return true;
}

void bitmap::set_pixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue) {
    uint8_t* pixel{ pixels_.get() };
    pixel += (y * 3) * width_ + (x * 3);

    pixel[0] = blue;
    pixel[1] = green;
    pixel[2] = red;
}

}
