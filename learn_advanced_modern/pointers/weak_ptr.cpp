#include <iostream>
#include <memory>

int main() {
    std::shared_ptr<int> sptr{std::make_shared<int>(9)};

    std::weak_ptr<int> wptrA{sptr};
    std::cout << wptrA.use_count() << std::endl; // does NOT increase the counter

    std::weak_ptr<int> wptrB(wptrA);
    std::cout << wptrA.use_count() << std::endl; // counter still the same

    if (!wptrB.expired()) {
        std::cout << "pointer still valid" << std::endl;
    }

    sptr = nullptr;
    if (wptrB.expired()) {
        std::cout << "nullptr" << std::endl;
    }

    return 0;
}
