#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

void save_file(const std::vector<int>& grades) {
    std::ofstream out_file("grades.txt");

    if (out_file.is_open()) {
        for (const auto grade : grades) {
            out_file << grade << ' ';
        }
        std::cout << "Grades written to a file!" << std::endl;
        out_file.close();
    } else {
        std::cout << "File write error!" << std::endl;
    }
}

std::vector<int> read_input() {
    std::vector<int> grades;

    std::cout << "Enter grades (q to quit)\n: ";
    int input;
    while (std::cin >> input && input >= 0) {
        grades.push_back(input);
        std::cout << "> ok, added\n: ";
    }
    std::cout << "> quit reading" << std::endl;
    save_file(grades);

    return grades;
}

void display_grades(const std::vector<int>& grades) {
    for (const auto grade : grades) {
        std::cout << grade << ' ';
    }
    std::cout << std::endl;
}

std::vector<int> read_file() {
    std::ifstream in_file("grades.txt");
    std::vector<int> grades;
    if (in_file.is_open()) {
        int grade;
        while (in_file >> grade) {
            grades.push_back(grade);
        }
    } else {
        std::cout << "Grades cannot be read!" << std::endl;
    }

    return grades;
}
int main() {
    std::vector<int> grades;
    
    char answer;
    std::cout << "Please secelt..." << std::endl;
    while (true) {
        std::cout << "(e)nter or (r)ead the grades?\n: ";
        answer = static_cast<char>(std::cin.get());
        if (answer == 'e') {
            grades = read_input();
            break;
        } else if (answer == 'r') {
            grades = read_file();
            break;
        }
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    if (grades.size()) {
        std::cout << "\nThe grades are: " << std::endl;
        display_grades(grades);

        std::cout << "\nAverage: " 
                  << (1.0 * std::accumulate(std::begin(grades), std::end(grades), 0) / grades.size())
                  << std::endl;

    }

    return 0;
}
