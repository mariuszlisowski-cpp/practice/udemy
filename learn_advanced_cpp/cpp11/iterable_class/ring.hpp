#ifndef RING_HPP
#define RING_HPP

#include <exception>
#include <iostream>
#include <vector>

template<typename T>                                        // compulsory here
class ring {
public:
    // nested class code
    class iterator;                                         // nested class declaration
    iterator begin() {
        return iterator(0, *this);                          // ring class object passed
    }
    iterator end() {
        return iterator(m_size, *this);                     // ring class object passed
    }

    // main class code
    ring(int size) : m_size(size) {
        m_values = new T[size];                             // allocate memory
    }
    ~ring() {
        delete[] m_values;                                  // deallocate memory
    }

    int size() {
        return m_size;
    }

    void add(T value) {
        m_values[m_pos++] = value;
        if (m_pos == m_size) {
            m_pos = 0;
        }
    }

    T& get(int pos) {
        return m_values[pos];                               // user's responsibility of 'out of range'
    }

private:
    T* m_values{};                                          // nullptr initialization
    int m_size;
    int m_pos{};                                            // zero initialization
};

template<typename T>
class ring<T>::iterator {                                   // nested & templated class definition
public:
    iterator() : m_pos{} {}
    iterator(int pos, ring& r) : m_pos(pos), m_ring(r) {}
    // prefix (++it)
    iterator& operator++() {
        ++m_pos;

        return *this;
    }
    // postfix (it++)
    iterator& operator++(int) {
        iterator temp{ *this };                             // save current state of the object
        ++m_pos;

        return *temp;
    }
    T& operator*() {
        return m_ring.get(m_pos);
    }
    bool operator!=(const iterator& other) const {
        return m_pos != other.m_pos;
    }

    // example only
    void test();                                           // method declaration (example)

private:
    int m_pos;
    ring& m_ring;                                           // avoiding copy
};

template<typename T>
void ring<T>::iterator::test() {                           // method definition (example)
    std::cout << "> nested class method" << std::endl;
}

#endif
