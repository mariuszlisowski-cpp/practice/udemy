#include <algorithm>
#include <cstring> // strlen()
#include <iostream>
#include <string>
#include <utility> // std::pair
#include <vector>

int main() {
    std::vector<std::string> words{"collection", "of", "words", "only"};

    // custom predicate (comparator)
    std::pair<std::vector<std::string>::iterator,
              std::vector<std::string>::iterator>
        pair_words = std::minmax_element(
            words.begin(), words.end(),
            [](const std::string &lhs, const std::string &rhs) {
                return lhs.size() < rhs.size();
            });

    std::cout << "> shortest word: " << *pair_words.first << std::endl;
    std::cout << "> longest word:  " << *pair_words.second << std::endl;

    return 0;
}
