#pragma once

#include "customer.hpp"

class Order {
public:
    Order(Customer customer) : customer_(customer) {}

    std::string print_summary() {
        return "Order by: " + customer_.name();
    }

private:
    Customer customer_;                                     // solid object
                                                            // lifetime limited to life of the order object
};
