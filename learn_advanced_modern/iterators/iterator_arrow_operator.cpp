#include <iostream>
#include <map>

int main() {
    std::map<char, int> letters {
        {'a', 5},
        {'b', 3},
        {'c', 4},
        {'e', 6}
    }; 

    std::map<char, int>::const_reverse_iterator cr_it = letters.crbegin();

    for ( ; cr_it != letters.crend(); ++cr_it) {
        std::cout << cr_it->first << " : " << cr_it->second << std::endl;
    }

    return 0;
}
