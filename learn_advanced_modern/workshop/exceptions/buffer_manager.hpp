#pragma once

class BufferManager {
  public:
    BufferManager(int size = 10);
    ~BufferManager();
    BufferManager(const BufferManager& other);
    BufferManager& operator=(const BufferManager& other);

    void populate_buffer(char* data);
    void print_buffer();

  private:
    int size;
    char* buffer;
};
