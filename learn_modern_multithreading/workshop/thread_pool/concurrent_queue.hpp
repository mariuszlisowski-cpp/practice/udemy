#ifndef CONCURRENT_QUEUE_HPP
#define CONCURRENT_QUEUE_HPP

#include <queue>
#include <mutex>
#include <condition_variable>

template <class T>
class concurrent_queue {
public:
	concurrent_queue() = default;
	void push(T value) {
		std::lock_guard<std::mutex> lg(m_);
		q_.push(value);
		cv_.notify_one();
	}

	void pop(T& value) {
		std::unique_lock<std::mutex> lg(m_);
		cv_.wait(lg, [this] {return !q_.empty();});
		value = q_.front();
		q_.pop();
	}

private:
	std::mutex m_;
	std::queue<T> q_;
	std::condition_variable cv_;
};

#endif
