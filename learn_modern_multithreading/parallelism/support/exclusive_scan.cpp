#include <execution>
#include <iostream>
#include <numeric>
#include <vector>

// using namespace std::execution;

int main() {
	std::vector<int> v {1, 2, 3, 4, 5, 6, 7, 8};
	std::vector<int> v2;

	// support execution policies (not in clang yet)
	std::exclusive_scan(v.begin(), v.end(),  std::back_inserter(v2), 0);	// replaces std::partial_sum
	
	std::cout << "Original vector elements: ";
	for (auto i : v) {
		std::cout << i << ", ";
	}
	
	std::cout << "\nExclusive scan vector elements: ";
	for (auto i : v2) {
		std::cout << i << ", ";
	}
}
