#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> numsA{3, 1, 4, 1, 5, 9};
    std::vector<int> numsB{3, 1, 4, 1, 5};

    // bool are_equal = std::equal(numsA.cbegin(), numsA.cend(), numsB.cbegin());
    bool are_equal = std::equal(numsA.cbegin(), numsA.cend(), numsB.cbegin(), numsB.cend()); // c++14
    std::cout << "> collections are " << (are_equal ? "the same" : "NOT the same") << std::endl;

    return 0;
}
