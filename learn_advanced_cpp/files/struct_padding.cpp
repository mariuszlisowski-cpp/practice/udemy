#include <iostream>

;
#pragma pack(push, 1)   // avoiding padding in a data structure
struct PersonA {
    char name[50];      // 50 bytes
    int age;            //  4 bytes
    double weight;      //  8 bytes
};
#pragma pack(pop)       // terminate packing (default padding)

struct PersonB {
    char name[50];      // 50 bytes
    int age;            //  4 bytes
    double weight;      //  8 bytes
};

int main() {
    std::cout << sizeof(PersonA) << std::endl;  // no padding
    std::cout << sizeof(PersonB) << std::endl;  // with padding (default)

    return 0;
}
