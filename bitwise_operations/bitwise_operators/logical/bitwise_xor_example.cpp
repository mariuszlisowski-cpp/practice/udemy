/* example of exclusive OR - symbol ^ */

#include <bitset>
#include <cstdint>
#include <iostream>

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1

   valueA 00001100
          XOR
   valueB 00011001
          ________
   result 00010101
    
   truth table:
   0 XOR 0 -> 0       both the same -> 0
   0 XOR 1 -> 1
   1 XOR 0 -> 1
   1 XOR 1 -> 0       both the same -> 0
*/

int main() {
    {
    /* binary */
    uint8_t valueA{0b00001100};                                // 12
    uint8_t valueB{0b00011001};                                // 25

    uint8_t result = valueA ^ valueB;                          // 21
    std::cout << std::bitset<8>(result) << std::endl;
     
    /* decimal */
    result = valueA ^ valueB;
    std::cout << std::bitset<8>(12 ^ 25)
              << std::endl << std::endl;
    }

    /* double XOR - giving the same result */
    uint8_t value{42};
    std::cout << std::bitset<8>(value) << std::endl;
    value ^= 24;
    value ^= 24;
    std::cout << std::bitset<8>(value) << std::endl;

    return 0;
}
