#include <iostream>

const std::string WORD_TO_MATCH{ "lion" };

struct Test {
    virtual bool operator()(std::string& str) = 0;
    virtual ~Test() {}
};

struct MatchWord : public Test {
    bool operator()(std::string& str) override {
        return str == WORD_TO_MATCH;
    }
};

// accepts any test
void check(std::string& str, Test& test) {
    std::cout << (test(str) ? "> matches" : "> does not match") << std::endl;
}

int main() {
    MatchWord match;
    std::string s{ "lion" };

    auto boolean{ match(s) };                                           // predicate behaves like a function
    std::cout << std::boolalpha << boolean << std::endl;

    Test& test{ match };                                                // initialized by lvalue child test
    check(s, test);

    return 0;
}
