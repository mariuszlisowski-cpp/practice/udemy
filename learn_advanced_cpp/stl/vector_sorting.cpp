#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

class Test {
public:
    Test(int id, const std::string& name) : id_(id), name_(name) {}

    void print() const {
        std::cout << id_ << ": " << name_ << std::endl;
    }

    bool operator<(const Test& other) {
        if (this != &other) {
            if (name_ == other.name_) {
                return id_ < other.id_;
            } else {
                return name_ < other.name_;
            }
        }

        return false;
    }

    friend bool comp(const Test& lhs, const Test& rhs);

private:
    int id_;
    std::string name_;
};

// compare by name and id
void print_vector(const std::vector<Test>& tests) {
    std::cout << "-------" << std::endl;
    for (size_t i = 0; i < tests.size(); ++i) {
        tests[i].print();
    }
}

// compare by name only
bool comp(const Test& lhs, const Test& rhs) {
    return lhs.name_ < rhs.name_;
}

int main() {
    std::vector<Test> tests;
    tests.push_back(Test(1, "Mike"));
    tests.push_back(Test(2, "Elen"));
    tests.push_back(Test(4, "Gary"));
    tests.push_back(Test(3, "Gary"));
    print_vector(tests);

    std::sort(tests.begin(), tests.end());                                  // operator< used
    print_vector(tests);

    std::sort(tests.begin(), tests.end(), comp);                            // comparator (funtion pointer)

    return 0;
}
