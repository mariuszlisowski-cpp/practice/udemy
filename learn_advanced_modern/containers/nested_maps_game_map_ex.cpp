#include <iostream>
#include <map>
#include <string>

class Coordinate {
public:
    Coordinate(int x, int y) : x_(x), y_(y) {}
    bool operator<(const Coordinate& rhs) const {
        if (this != &rhs) {
            return this->x_ < rhs.x_;
        }

        return false;
    }
private:
    int x_{};
    int y_{};
};

class Item {
public:
    Item(std::string name, size_t power) : name_(name), power_(power) {}
    std::string get_name() const {
        return name_;
    }
    size_t get_power() const {
        return power_;
    }
private:
    std::string name_{};
    size_t power_{};
};

class Level {
public:
    Level(size_t number) : number_(number) {}
    Level(size_t number, std::string difficulty)
        : number_(number)
        , difficulty_(difficulty) {}
    bool operator<(const Level& rhs) const {
        if (this != &rhs) {
            return this->number_ < rhs.number_;
        }

        return false;
    }
    size_t get_number() const {
        return number_;
    }
    std::string get_difficulty() const {
        return difficulty_;
    }
private:
    size_t number_;
    std::string difficulty_;
};

using level_map = std::map<Coordinate, Item>;

void print_game_map(const std::map<Level, level_map>& game);

int main() {
    level_map first_level_items{
        {Coordinate{1,5}, Item{"Player", 4}},
        {Coordinate{2,8}, Item{"Ghost", 3}}
    };

    level_map second_level_items{
        {Coordinate{-1,9}, Item{"Knight", 9}},
        {Coordinate{2,7}, Item{"Witch", 7}},
    };

    std::map<Level, level_map> game_map;
    game_map.insert({Level(1, "easy"), first_level_items});
    game_map.insert({Level(2, "medium"), second_level_items});

    print_game_map(game_map);

    auto it = game_map.find({2}); // level 2
    if (it != game_map.end()) {
        std::cout << "> erased" << std::endl;
        it->second.erase({2,7});  // 'Witch'
    }
    print_game_map(game_map);

    return 0;
}

void print_game_map(const std::map<Level, level_map>& game) {
    for (const auto& level : game) {
        std::cout << level.first.get_number() << ' ' << level.first.get_difficulty() << ": ";
        for (const auto& item : level.second) {
            std::cout << item.second.get_name() << ", ";
        }
        std::cout << std::endl;
    }
}
