#include <algorithm>
#include <iostream>
#include <string>

int main() {
    char delimiter{'!'};
    std::string str{"print till here!and then the rest"};

    std::string::iterator it = std::find(str.begin(), str.end(), delimiter);
    
    auto pos = std::distance(str.begin(), it);
    std::string sub_left = str.substr(0, pos);
    std::string sub_right = str.substr(pos + 1, str.size() - 1);
    
    if (it != str.end()) {
        std::cout << sub_left << std::endl;
        std::cout << sub_right << std::endl;
    }

    return 0;
}
