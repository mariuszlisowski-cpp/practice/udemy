#include <iostream>

class Refrigerator {
public:
    Refrigerator(double temp, bool door = true, bool power = false)
        : temperature_(temp), door_open_(door), power_on_(power) {}

    // streams cannot be passed by value as they are not copyable
    std::ostream& print(std::ostream& os) const {
        os << "> temperatue: " << temperature_  << std::endl
           << "> door is " << (door_open_ ? "open" : "closed") << std::endl
           << "> powered: " << (power_on_ ? "yes" : "no") << std::endl << std::endl;

        return os;
    }

    // no friendship needed because of above method 
    // friend std::ostream& operator<<(std::ostream& os, const Refrigerator& refrigerator);

private:
    double temperature_;
    bool door_open_;
    bool power_on_;
};

// return type must be a reference as it can be chained: operator<<(operator<<(stream, i), j)
// thus it returns a reference as an input for another call
std::ostream& operator<<(std::ostream& os, const Refrigerator& refrigerator) {
    refrigerator.print(os); // use of ready method instead of implementing

    return os;
}

int main() {
    Refrigerator refrigeratorA(20);
    Refrigerator refrigeratorB(3.9, false, true);

    // The next call in the chain needs to modify the stream by
    // pushing data on it, so it must be a modifiable reference
    std::cout << refrigeratorA << refrigeratorB << std::endl;


    return 0;
}
