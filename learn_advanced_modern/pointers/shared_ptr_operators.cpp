#include <iostream>
#include <memory>

struct Point {
    int x;
    int y;
};

int main() {
    std::shared_ptr<Point> sptr{std::make_shared<Point>(Point{-9, 7})};
    
    // dereference value
    Point point = *sptr;

    // dereference member
    int x = sptr->x;
    int y = sptr->y;

    // bool & negation operator
    if (!sptr) {
        std::cerr << "nullptr";
    }

    // copy operator
    auto sh_ptr(sptr);

    // assignment operator
    std::shared_ptr<Point> s_ptr;
    s_ptr = sptr;
    
    return 0;
}
