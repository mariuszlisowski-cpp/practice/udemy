#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <typename T> void print_vector(const std::vector<T> vec) {
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(std::cout, " "));
}

int main() {
    std::vector<int> in{-1, 2, 3, 4, 5};
    std::vector<int> out;

    auto predicate = [](int n)->bool { return n < 0; }; // bool redundant
    std::copy_if(in.begin(), in.end(), std::back_inserter(out), predicate);
    print_vector(out);

    return 0;
}
