#include <iostream>
#include <memory>

class Foo {};

int main() {
    std::shared_ptr<Foo> s_ptr{ std::make_shared<Foo>() };

    /* convert shared to weak */
    std::weak_ptr<Foo> w_ptr{ s_ptr };                              // convert to weak

    /* convert weak to shared */
    std::shared_ptr<Foo> ptrA{ w_ptr.lock() };                      // convert back to shared
                                                                    // empty if deleted earlier
    auto ptrB{ std::shared_ptr<Foo>(w_ptr) };                       // throw exception if empty
                                                                    // std::bad_weak_ptr
    
    return 0;
}
