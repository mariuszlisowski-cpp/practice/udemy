#include <cstddef>
#include <iostream>
#include <memory>

int main() {
    // original
    std::shared_ptr<int> sptrA;
    std::cout << "> count A: " << sptrA.use_count() << std::endl;

    sptrA = std::make_shared<int>(77);                           // memory allocated
    std::cout << "> count A: " <<  sptrA.use_count() 
              << std::endl << std::endl;

    // another shared
    std::shared_ptr<int> sptrB;
    sptrB = sptrA; // B points to the same object as A
    std::cout << "> count A: " << sptrA.use_count() << std::endl; // count incremented
    std::cout << "> count B: " << sptrB.use_count() << std::endl; // same object's member function called

    // copy left
    sptrA = nullptr; // original destroyed (now point to null)
    std::cout << "> count A: " << sptrA.use_count() << std::endl; // count zero (as null)
    std::cout << "> count B: " << sptrB.use_count() << std::endl; // count decremented

    sptrB = nullptr;                                              // memory released

    return 0;
}
