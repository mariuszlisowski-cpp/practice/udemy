#include <thread>
#include <mutex>
#include <iostream>

std::mutex mutex1;
std::mutex mutex2;

void func1() {
	std::cout << "Thread 1 locking mutexes 1 & 2..." << std::endl;
	std::scoped_lock lk1(mutex1, mutex2);
	std::cout << "> thread 1 has locked mutexes 1 & 2" << std::endl;

	std::cout << "# thread 1 releases locks" << std::endl;
}

void func2() {
	std::cout << "Thread 2 locking mutexes 2 & 1..." << std::endl;
	std::scoped_lock lk1(mutex2, mutex1);                               // reversed order
	std::cout << "> thread 2 has locked mutexes 2 & 1" << std::endl;

	std::cout << "# thread 2 releases locks" << std::endl;
}

int main() {
    // deadlock prevented
	std::thread t1{ func1 };
	std::thread t2{ func2 };

	t1.join();
	t2.join();
}
