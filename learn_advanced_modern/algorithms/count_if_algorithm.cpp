#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    int value_to_count{1};
    std::vector<int> nums{1, 4, 1, 3, 1, 1, 9, 2};
    
    auto counter = std::count(nums.begin(), nums.end(), value_to_count);
    std::cout << "> value " << value_to_count << " found " << counter << " time(s)" << std::endl;

    // only odd nums
    int odd_counter = std::count_if(nums.begin(), nums.end(), [](int val){ return val % 2; });
    int even_counter = std::count_if(nums.begin(), nums.end(), [](int val){ return !(val % 2); });
    std::cout << "> found " << odd_counter << " odd and " << even_counter << " even number(s)" << std::endl;


    return 0;
}
