#include <iostream>

class Clazz {
public:
    Clazz() {}
    Clazz(int value) : value_(value) {}
    
    // same as compiler generated assignment operator (would be good enough here)
    // may be chained thus returns a reference
    Clazz& operator=(const Clazz& other) {
        // if self-assignment do nothig
        if (this != &other) {
            value_ = other.value_;
            std::cout << "> assignment operator used" << std::endl;
        }

        return *this;
    }

    int getValue() {
        return value_;
    }
private:
    int value_;
};

int main() {
    Clazz c1(9);
    Clazz c2;
    Clazz c3;

    c1 = c1;      // self-assignment (skipped)
        
    c2 = c1;      // assignment used here (equivalent to c2.operator=(c1) )
    std::cout << c2.getValue() << std::endl;
    
    // chained assignment
    c3 = c2 = c1; // used here TWICE (equivalent to c3.operator=(c2.operator=(c1)) )
    std::cout << c3.getValue() << std::endl;


    return 0;
}
