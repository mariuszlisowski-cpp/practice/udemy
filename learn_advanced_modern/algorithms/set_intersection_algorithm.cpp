#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int main() {
    std::vector<std::string> textA{"Ala", "ma", "kota", "co", "Ela", "nie", "ma", "psa"};
    std::vector<std::string> textB{"ala", "ma", "kota", "bo", "ela", "nie", "ma", "psa"};

    std::cout << "> common words in containers:" << std::endl;
    std::set_intersection(textA.begin(), textA.end(),
                          textB.begin(), textB.end(),
                          std::ostream_iterator<std::string>(std::cout, ", "));

    return 0;
}
