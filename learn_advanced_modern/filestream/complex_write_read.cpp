#include <fstream>
#include <iostream>

class Data {
    int key{9};     // 4 bytes
    int value{5};   // 4 bytes
};

int main() {
    Data x; // size of 8 (2 x 4 bytes)
    std::cout << sizeof(Data) << std::endl;

    Data* y = new Data[8];

    std::fstream myFile("binary", std::ios::in | std::ios::out | std::ios::binary);
    myFile.clear();
    myFile.seekp(0); // put pointer
    myFile.write((char*)& x, sizeof(Data)); // cast an object to an array (char*)

    myFile.seekg(std::ios::beg); // get pointer
    myFile.read((char*) y, sizeof(Data) * 10);

    return 0;
}
