#include <fstream>
#include <ios>
#include <iostream>
#include <string>

int main() {
    std::fstream file_out;                                          // write & read
    
    std::string filename{"test"};
    file_out.open(filename, std::ios::out);
    if (file_out.is_open()) { 
        file_out << "first line written to a file" << std::endl;    // std::endl flushes stream
        file_out << "second line written to a file" << std::endl;   // instead of std::flush(file_out)
        file_out.close();
    } else {
        std::cout << "Error creating a file: " << filename << std::endl;
    }

    return 0;
}
