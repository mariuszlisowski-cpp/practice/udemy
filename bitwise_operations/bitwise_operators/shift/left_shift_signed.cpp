#include <bitset>
#include <iostream>

using namespace std;

int main() {
    short value = 16384;
    cout << value << '\t'
         << bitset<16>(value) << endl;          // 0b01000000'00000000

    /* most significant bit is a sign bit */
    short result = value << 1;                      // 16384 left-shifted by 1 = -32768
    cout << result << '\t'
         << bitset<16>(result) << endl;

    return 0;
}
