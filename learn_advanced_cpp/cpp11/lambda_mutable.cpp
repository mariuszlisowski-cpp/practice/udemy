#include <iostream>

int main() {
    int cats{ 5 };

    [cats]() mutable {                              // capture by value
        std::cout << ++cats << std::endl;           // variable incremented
    };

    return 0;
}
