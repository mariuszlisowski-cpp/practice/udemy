#include <string>

/*  The rule of five:
    If you need to explicitly declare either 
    - the destructor
    - copy constructor
    - copy assignment operator
    - move constructor
    - move assignment operator
    you probably need to explicitly declare all three of them
*/

class person {
  public:
    person(const std::string& name, int age);       // Constructor

    ~person() noexcept = default;                   // 1/5: Destructor
    person(const person&) = default;                // 2/5: Copy Constructor
    person& operator=(const person&) = default;     // 3/5: Copy Assignment Operator
    person(person&&) noexcept = default;            // 4/5: Move Constructor
    person& operator=(person&&) noexcept = default; // 5/5: Move Assignment Operator

  private:
    std::string name;
    int age;
};

int main() { return 0; }
