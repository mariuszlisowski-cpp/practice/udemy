#include <iostream>
#include <string>

int main() {
    std::string strA{"ala"};
    std::string strB{"kot"};

    // copying
    std::string temp{strA};     // copy constructor
    strA = strB;                // assignment operator (lhs changed)
    strB = temp;                // assignment operator (lhs changed)
    
    // moving
    std::swap(strA, strB);      // both exchanged (but copying changes lhs only)

    return 0;
}
