#include <future>
#include <iostream>
#include <string>
#include <thread>

void test(const std::string& str, double d) {
    std::cout << str << ' ' << d << std::endl;
}

int main() {
    std::packaged_task<void(const std::string&, double)> pt(test);        // signature of the function

    std::future<void> f(pt.get_future());

    std::thread th(std::move(pt), "abc", 3.14);
    th.join();

    return 0;
}
