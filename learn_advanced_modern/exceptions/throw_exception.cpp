#include <iostream>
#include <stdexcept>

void positive_only(int value) {
    if (value < 0) {
        throw std::runtime_error("> negative numbers not allowed!");
    }
}

int main() {
    try {
        positive_only(-1);
    } catch (const std::runtime_error& e) {
        std::cout << e.what() << std::endl;
    }
    
    return 0;
}
