#include <atomic>
#include <ios>
#include <iostream>
#include <thread>

std::atomic_flag boolean = ATOMIC_FLAG_INIT;                // must be initialized (to false)

void test() {
    boolean.clear();                                        // set to false
    bool t = boolean.test_and_set();                        // set to true and return the previous value
    std::cout << std::boolalpha << t << std::endl;
}

int main() {
    test();

    return 0;
}
