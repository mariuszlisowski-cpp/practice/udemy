#include <fstream>
#include <iostream>
#include <string>

;
#pragma pack(push, 1)                                                       // no padding
struct Person {
    char name[50];
    int age;
    double height;
};
#pragma pack(pop)

int main() {
    Person personA{"Elen", 33, 160};                                        // initialized structure

    // write binary
    std::string filename{"test.bin"};
    std::ofstream out_file;                                                 // out object
    out_file.open(filename, std::ios::binary);                              // binary mode
    if (out_file.is_open()) {
        out_file.write(reinterpret_cast<char*>(&personA), sizeof(personA));
        out_file.close();
    } else {
        std::cout << "Error writing a file: " << filename << std::endl;
    }

    // read binary
    Person personB{};
    std::ifstream in_file;                                                  // in object
    in_file.open(filename, std::ios::binary);                               // binary mode
    if (in_file.is_open()) {
        in_file.read(reinterpret_cast<char*>(&personB), sizeof(Person));
        in_file.close();
    } else {
        std::cout << "Error reading a file: " << filename << std::endl;
    }

    std::cout << personB.name << std::endl;

    return 0;
}
