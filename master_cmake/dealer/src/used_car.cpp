#include "../inc/used_car.hpp" // CMake: no need for relative path as include directory known (IDE compliant)

Used_car::Used_car(const std::string& make, const std::string& model, unsigned int year, unsigned mileage)
        : Car(make, model)
{
    year_ = year;
    mileage_ = mileage;
}        

std::ostream& operator<<(std::ostream& os, const Used_car& car) {
    os << "A used car was sold:"
        << "\n> make:    " << car.make_
        << "\n> model:   " << car.model_
        << "\n> year:    " << car.year_
        << "\n> mileage: " << car.mileage_ << std::endl;

    return os;
}
