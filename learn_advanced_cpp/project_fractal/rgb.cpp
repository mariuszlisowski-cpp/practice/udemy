#include "rgb.hpp"

namespace raisin {

rgb::rgb(double red, double green, double blue) : red_(red), green_(green), blue_(blue) {}

rgb& rgb::operator-(const rgb& other) {
    this->red_ -= other.red_;
    this->green_ -= other.green_;
    this->blue_ -= other.blue_;

    return *this;
}

}
