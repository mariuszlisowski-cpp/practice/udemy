#include <iostream>
#include <string>
#include <vector>

// receiving argument by value
void test(std::vector<std::string> vec) {
    // no-op
}

int main() {
    std::vector<std::string> v{1000000};
    
    test(v);                                // passing by value (copying)
    std::cout << v.size() << std::endl;

    test(std::move(v));                     // passing as rvalue to the function (no copying)
    std::cout << v.size() << std::endl;     // empty collection (as has been moved)

    return 0;
}
