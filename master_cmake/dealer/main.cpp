#include "inc/car.hpp"
#include "inc/used_car.hpp"

int main() {
    Car new_car("Nissan", "Leaf");
    Used_car used_car("Subaru", "Forester", 2008, 160500);

    std::cout << new_car << std::endl;
    std::cout << used_car << std::endl;
    
    return 0;
}
