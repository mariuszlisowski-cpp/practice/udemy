#include <iostream>
#include <string>
#include <thread>

void hello() {
    std::cout << "Hello thread!" << std::endl;
}
    
void hello_arg_const(const std::string& str) {
    std::cout << str << std::endl;
    
}

void hello_arg_mutable(std::string& str) {
    str = "Altered string";
    std::cout << str << std::endl;
    
}

void hello_arg_rvalue(std::string&& str) {
    std::cout << "Ovnership transferred of string " << str << std::endl;
}

int main() {
    std::string s{"test"};

    // threads starting immediately 
    std::thread th1(hello);                                 // callable object (pointer to the function as a name)
    std::thread th2(hello_arg_const, "Argument thread");    // callable object (cannot be overloaded)
    std::thread th3(hello_arg_mutable, std::ref(s));        // cast to reference
    std::thread th4(hello_arg_rvalue, std::move(s));        // cast to rvalue
    
    // std::cout << "Empty string: " << s << std::endl;     // NOT SAFE to read (not valid state)
    s = "not empty";                                        // safe to write

    th1.join();
    th2.join();
    th3.join();
    th4.join();

    return 0;
}
