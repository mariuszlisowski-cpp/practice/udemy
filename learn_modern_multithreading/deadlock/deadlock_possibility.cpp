#include <thread>
#include <mutex>
#include <iostream>

std::mutex mutex1;
std::mutex mutex2;

void func1() {
	std::cout << "Thread 1 locking mutex 1..." << std::endl;
	std::unique_lock lk1(mutex1);				                // acquire lock on mutex1
	std::cout << "> thread 1 has locked mutex 1" << std::endl;

	std::cout << "Thread 1 locking mutex 2..." << std::endl;
	std::unique_lock lk2(mutex2);				                // wait for lock on mutex2
	std::cout << "> thread 1 has locked mutex 2" << std::endl;

	std::cout << "Thread 1 releases locks" << std::endl;
}

void func2() {
	std::cout << "Thread 2 locking mutex 2..." << std::endl;
	std::unique_lock lk1(mutex2);	            		        // acquire lock on mutex2
	std::cout << "> thread 2 has locked mutex 2" << std::endl;

	std::cout << "Thread 2 locking mutex 1..." << std::endl;
	std::unique_lock lk2(mutex1);	                    		// wait for lock on mutex1
	std::cout << "> thread 2 has locked mutex 1" << std::endl;

	std::cout << "Thread 2 releases locks" << std::endl;
}

int main() {
    // deadlock possible (threads waiting for each other)
	std::thread t1{ func1 };
	std::thread t2{ func2 };

	t1.join();
	t2.join();
}
