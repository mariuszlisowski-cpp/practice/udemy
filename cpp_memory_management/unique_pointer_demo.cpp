#include "leak/test.hpp"

#include <iostream>
#include <memory>

std::unique_ptr<Test> create() {
    return std::make_unique<Test>();                        // created here
}

void make_talk_ref(std::unique_ptr<Test>& ptr) {
    ptr.reset();                                            // risk of modifying content
}

void make_talk(std::unique_ptr<Test> ptr) {                 // creating a local copy
    ptr->talk();                                            // ..or accepting moved object
}                                                           // deleted here

void use_unique_ptr() {
    auto ptr{ create() };
    make_talk(std::move(ptr));                              // ownership moved to overloaded function
}                                                           // notihing to delete

int main() {
    /* ways of creating */
    {
    auto j{ std::make_unique<Test>() };                     // c++14
    auto k{ std::unique_ptr<Test>(new Test()) };            // using a constructor
    k.reset(new Test());                                    // reuse existing (delete previous object)
    }                                                       // deleting both remaining
    
    /* using */
    use_unique_ptr();

    auto risky{ std::make_unique<Test>() };
    make_talk_ref(risky);
    if (!risky) {
        std::cout << "> no object pointed" << std::endl;
    }

    return 0;
}
