#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>

int main() {
	std::multimap<int, double> mmap{
		{1, 1.1},
		{2, 2.1},
		{2, 2.2},
		{2, 2.3},
		{8, 8.1},
		{9, 9.1},
	};

	// all values corresponding to the key
	const int find_me = 2;
	auto lower_it = mmap.lower_bound(find_me);
	auto upper_it = mmap.upper_bound(find_me);
	for (auto it = lower_it; it != upper_it; ++it) {
		std::cout << it->second << ' ';
	}
	std::cout << std::endl;
	// or simpler
	for(auto it = mmap.lower_bound(find_me); it != mmap.upper_bound(find_me); ++it) {
		std::cout << it->second << ' ';
	}
	std::cout << std::endl;

	// find non existent key
	lower_it = mmap.lower_bound(3); // returns an iterator to the first element that is greater than its argument
	std::cout << lower_it->first << " : " << lower_it->second << std::endl;

	// find non existent key
	upper_it = mmap.upper_bound(2); // returns an iterator to the first element that is greater than its argument
	std::cout << upper_it->first << " : " << upper_it->second << std::endl;

	return 0;
}
