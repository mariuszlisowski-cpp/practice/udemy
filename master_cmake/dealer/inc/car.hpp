#ifndef CAR_HPP
#define CAR_HPP

#include <boost/date_time.hpp>
#include <ostream>

class Car {
public:
    Car(const std::string& make, const std::string& model);

    friend std::ostream& operator<<(std::ostream& os, const Car& car);

protected:
    std::string make_;
    std::string model_;
    unsigned int year_;
    unsigned int mileage_;

    boost::posix_time::ptime time_;
};

#endif
