#include <iostream>

// function returning lambda functor
auto generate_payrise(double rate) {
    return [rate](double salary) {
               return salary * rate; // returns double type
           };
}

int main() {
    double basic_salary{5000};
    double boss_salary{10000};

    // functor assignment
    auto calculate_pay_salary_5_percent_increase = generate_payrise(1.05); // raise by 10% (captured variable)

    double increased_salary = calculate_pay_salary_5_percent_increase(basic_salary); // lambda argument
    std::cout << increased_salary << std::endl;

    // another functor
    auto calculate_salary_10_percent_increase = generate_payrise(1.1); // raise by 10% (captured variable)

    double increased_boss_salary = calculate_salary_10_percent_increase(boss_salary); // lambda argument
    std::cout << increased_boss_salary << std::endl;

    return 0;

}
