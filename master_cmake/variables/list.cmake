# script mode: cmake -P <filename>
cmake_minimum_required(VERSION 3.0.0)

# list(<subcommand> <list_name> <args> <return_variable>)              
# e.g. APPEND, REMOVE_ITEM, REMOVE_AT, INSERT, REVERSE, REMOVE_DUPLICATES, FIND, GET, JOIN, SORT

set(VAR a b c;d "e;f" 3.14 "hello there")
# index 0 1 2 3  4 5  6     7
#      -8-7-6-5 -4-3 -2    -1

# altering variable
list(APPEND VAR 1.6 word)                  # two strings appended (at the end)
message(${VAR})

list(REMOVE_AT VAR 2 -3)                   # remove at indexes 2 and -3
message(${VAR})

list(INSERT VAR 2 item 1.6)                # insert at index 2
message(${VAR})

list(REVERSE VAR)
message(${VAR})

list(REMOVE_DUPLICATES VAR)
message(${VAR})

list(SORT VAR)
message(${VAR})

# returning a value
list(LENGTH VAR LEN_VAR)                    # no of elements
message(${LEN_VAR})

list(GET VAR 2 5 7 SUB_LIST1)               # get specified indexes
message(${SUB_LIST1})

list(SUBLIST VAR 2 3 SUB_LIST2)             # get 3 items starting at index 2
message(${SUB_LIST2})

list(JOIN VAR _ STR_VAR)                    # join using a delimiter
message(${STR_VAR})

list(FIND VAR item FIND_VAR)                # returns index
message(${FIND_VAR})

