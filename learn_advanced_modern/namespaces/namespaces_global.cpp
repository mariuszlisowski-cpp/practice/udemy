#include <iostream>

namespace Alpha {
class Test {};
}

namespace Beta {
class Test {};
}

class Test {};

// split over different parts (even different files)
namespace Alpha {
class Another_test {}; 
}

int main() {
    Alpha::Test testingAlpha;
    Beta::Test testingBeta;

    ::Test testingGlobal;                   // global namespace (:: is a scope operator)

    Alpha::Another_test testingAnotherAlpha;

    return 0;
}
