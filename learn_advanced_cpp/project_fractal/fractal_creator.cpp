#include "fractal_creator.hpp"

namespace raisin {

fractal_creator::fractal_creator(int width, int height)
    : width_(width)
    , height_(height)
    , histogram_(std::make_unique<int[]>(mandelbrot::MAX_ITERATIONS))
    , fractal_(std::make_unique<int[]>(width_ * height_))
    , bmp_(width_, height_)
    , z_list_(width_, height_)
    {
        z_list_.add(zoom(width_ / 2, height_ / 2, 4.0 / width_));
    }

fractal_creator::~fractal_creator() {}

void fractal_creator::run(const std::string& filename) {
    calculate_iteration();
    calculate_total_iterations();
    draw_fractal();
    write_bitmap(filename);
}

void fractal_creator::add_zoom(const zoom& z) {
    z_list_.add(z);
}

void fractal_creator::add_range(double range_end, const rgb& color) {
    ranges_.push_back(static_cast<int>(range_end * mandelbrot::MAX_ITERATIONS));
    colors_.push_back(color);
    
    if (got_first_range) {
        range_totals_.push_back(0);
    }
    got_first_range = true;
}

void fractal_creator::calculate_iteration() {
    for (int y = 0; y < height_; ++y) {
        for (int x = 0; x < width_; ++x) {
            std::pair<double, double> coords{ z_list_.do_zoom(x, y) };

            int iterations{ mandelbrot::get_iterations(coords.first, coords.second) };
            fractal_[y * width_ + x] = iterations;

            if (iterations != mandelbrot::MAX_ITERATIONS) {
                histogram_[iterations]++;
            }
        }
    }
}

void fractal_creator::calculate_total_iterations() {
    for (int i = 0; i < mandelbrot::MAX_ITERATIONS; ++i) {
        total_ += histogram_[i];
    }
}

void fractal_creator::draw_fractal() {
    rgb start_color(0, 0, 20);
    rgb end_color(255, 128, 74);
    rgb diff_color{ end_color - start_color };

    for (int y = 0; y < height_; ++y) {
        for (int x = 0; x < width_; ++x) {
            uint8_t red{};
            uint8_t green{};
            uint8_t blue{};

            int iterations{ fractal_[y * width_ + x] };
            if (iterations != mandelbrot::MAX_ITERATIONS) {            
                double hue{};
                for (int i = 0; i <= iterations; ++i) {
                    hue += static_cast<double>(histogram_[i]) / total_;
                }
                red = static_cast<uint8_t>(start_color.red_ + diff_color.red_ * hue);
                green = static_cast<uint8_t>(start_color.green_ + diff_color.green_ * hue);
                blue = static_cast<uint8_t>(start_color.blue_ + diff_color.blue_ * hue);
            }
            bmp_.set_pixel(x, y, red, green, blue);
        }
    }
}

void fractal_creator::write_bitmap(const std::string& name){
    bmp_.write(name);
}

void fractal_creator::calculate_range_totals() {
    int range_index{};
    for (int i{ 0 }; i < mandelbrot::MAX_ITERATIONS; ++i) {
        int pixels{ histogram_[i]};
        if (i >= ranges_[range_index +1]) {
            ++range_index;
        }
        range_totals_[range_index] += pixels;
    }
}

}
