#include <iostream>
#include <queue>

template<typename T>
void print_front_back(std::queue<T>& q) {
    std::cout << "> front: " << q.front() << std::endl;
    std::cout << "> back: " << q.back() << std::endl;
}

int main() {
    // FIFO (First In First Out) container adapter
    std::queue<int> q({1, 2, 3}); // thus such initializer list

    q.push(4);
    print_front_back(q);

    q.pop(); // pops at front
    print_front_back(q);

    if (!q.empty()) {
        std::cout << "> size: " << q.size() << std::endl;
    }

    /* no iterators 
    for (auto el : q) {} // error */

    return 0;
}
