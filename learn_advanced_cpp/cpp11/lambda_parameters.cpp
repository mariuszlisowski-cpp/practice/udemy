#include <iostream>
#include <string>

void test(void (*functor)(const std::string&)) {
    functor("Peggy");
}

auto testDivide(double (*funct_ptr)(double, double)) {
    return funct_ptr(4, 0);
}

int main() {
    auto function_ptr = [](const std::string& name) {
        std::cout << "Hi " << name << "!" << std::endl;
    };

    function_ptr("Alice");                                              // using a pointer to call
    test(function_ptr);                                                 // function accepting functor
    test([](const std::string& name) { std::cout << "Bye!\n"; });       // direct lambda use

    auto pDivide = [](double a, double b) -> double {                   // redundant return type
        return b ? a / b : 0;
    };

    std::cout << pDivide(3, 2) << std::endl;
    std::cout << testDivide(pDivide) << std::endl;


    return 0;
}
