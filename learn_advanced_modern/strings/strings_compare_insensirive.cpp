#include <cctype>
#include <iostream>
#include <string>

bool equal_strings_case_insensitive(const std::string& lhs, const std::string& rhs) {
    if (lhs.size() != rhs.size()) {
        return false;
    }
    auto l_it = lhs.cbegin();
    auto r_it = rhs.cbegin();
    while (l_it != lhs.cend() && r_it != rhs.cend()) {
        if (std::toupper(*l_it) != std::toupper(*r_it   )) {
            return false;
        }
        ++l_it;
        ++r_it;
    }
    
    return true;
}

int main() {
    std::string strA("Hello, world!");
    std::string strB("Hello, WORLD!");

    std::cout << (equal_strings_case_insensitive(strA, strB) ? "Same strings!" : "Not the same!") 
              << std::endl;

    return 0;
}
