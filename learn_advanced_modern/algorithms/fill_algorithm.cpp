#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

template <class T>
void print_container(const std::vector<T>& cont) {
    std::for_each(cont.begin(), cont.end(),
                  [](const T& s) { std::cout << s << ' '; });
    std::cout << std::endl;
}

int main() {
    constexpr size_t size = 5;
    std::vector<std::string> v(size);
    
    std::fill(v.begin(), v.end(), "filled");
    print_container(v);

    return 0;
}
