#include <iostream>

class Refrigerator {
public:
    Refrigerator(double temp, bool door = true, bool power = false)
        : temperature_(temp), door_open_(door), power_on_(power) {}

    // friendship can be avoided by implementing member functions
    // and calling them from within global operator<</>> methods
    friend std::ostream& operator<<(std::ostream& os, const Refrigerator& refrigerator);
    friend std::istream& operator>>(std::istream& is, Refrigerator& refrigerator);

private:
    double temperature_;
    bool door_open_;
    bool power_on_;
};

// return type must be a reference as it can be chained: operator<<(operator<<(stream, i), j)
// thus it returns a reference as an input for subsequent call
std::ostream& operator<<(std::ostream& os, const Refrigerator& refrigerator) {
    os << "> temperatue: " << refrigerator.temperature_  << std::endl
        << "> door is " << (refrigerator.door_open_ ? "open" : "closed") << std::endl
        << "> powered: " << (refrigerator.power_on_ ? "yes" : "no") << std::endl << std::endl;

    return os;
}

// stream& argument cannot be const at it is updated in a method
// same with the second argument (Refrigerator&)
std::istream& operator>>(std::istream& is, Refrigerator& refrigerator) {
    double temperature;
    std::cout << "Refrigerator temperature?\n: ";
    if (is >> temperature) {
        refrigerator.temperature_ = temperature;
    } else {
        std::cout << "> input error!" << std::endl;
    }

    return is;
}

int main() {
    Refrigerator refrigeratorA(20);
    Refrigerator refrigeratorB(3.9, false, true);

    // usage of operator>>
    std::cin >> refrigeratorA;

    // usage of operator<<
    std::cout << refrigeratorA << std::endl;


    return 0;
}
