#include <algorithm>
#include <map>
#include <iostream>
#include <string>
#include <utility>	// std::make_pair()

template<typename T, typename K>
void print_map(std::map<T, K> map) {
	std::for_each(map.begin(), map.end(), 
	              [](const std::pair<T, K>& pair) {
					  std::cout << pair.first << " : " << pair.second << std::endl;
				  });
}

template<typename T, typename K>
void print_map_modern(std::map<T, K> map) {
	for (auto [key, value]: map) { 		// c++17
		std::cout << key << " : " << value << std::endl;
	}
}

void check_insertion_status(const std::pair<std::map<char, int>::iterator, bool>& res) {
	if (res.second) {
		std::cout << "> inserted successfully: " << res.first->first << std::endl;
	} else {
		std::cout << "> duplicated value rejected: " << res.first->first << " exists!" << std::endl;
	}
}

int main() {
	// unique key elements (with corresponding values) strored in order (using < operator)
	std::map<char, int> map{ {'a', 1}, {'b', 2} }; // implemented as a tree
	print_map(map);

    std::pair<std::map<char, int>::iterator, bool> pair;
	pair = map.insert(std::make_pair('c', 3));
    check_insertion_status(pair);

	pair = map.insert({'d', 4}); // initializer list (success)
    check_insertion_status(pair);
	pair = map.insert({'d', 4}); // failure
    check_insertion_status(pair);

	// subscripting (not recommended)
	map['e'] = 6;						// creates a new pair in a map
	map['e'] = 5;						// changes the key's value (overwrites a value)
	int value = map['c'];				// fetches the key's value

	std::cout << "> fetched: " << value << std::endl;
	print_map_modern(map);
	
	char find_me = 'e';
	auto it = map.find(find_me);
	std::cout << find_me
	          << (it != map.end() ? " found: " + std::to_string(it->second): " not found!")
			  << std::endl;

	return 0;
}
