#include <iostream>

unsigned doubleValue_shift(unsigned value) {
    return value << 1;
}

/* same as above */
unsigned doubleValue(unsigned value) {
    return value *= 2;
}

int main() {
    auto value{250u};

    std::cout << doubleValue_shift(value) << std::endl;
    std::cout << doubleValue(value) << std::endl;

    return 0;
}
