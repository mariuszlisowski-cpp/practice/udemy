#include <iostream>
#include <string>

void might_go_wrong() {
    bool error_int = false;
    bool error_cstr = true;
    bool error_str = true;
    if (error_int) {
        throw 8;
    }
    if (error_cstr) {
        throw "c string thrown";
    }
    if (error_str) {
        throw std::string("string thrown");
    }
}

void use_might_go_wrong() {
    might_go_wrong();                                      // not caught here
}

int main() {
    try {
        use_might_go_wrong();                              // caught here
    } catch (int error_code) {
        std::cout << error_code << std::endl;
    } catch (const char* error_cstr) {
        std::cout << error_cstr << std::endl;
    } catch (std::string& error_message) {
        std::cout << error_message << std::endl;
    }

    std::cout << "> exitting normally" << std::endl;

    return 0;
}
