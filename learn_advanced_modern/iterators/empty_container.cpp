#include <iostream>
#include <vector>

int main() {
    std::vector<int> empty{};

    if (empty.begin() == empty.end()) {
        std::cout << "Container is empty!" << std::endl;
    }

    // nothing to do in a loop as begin() == end()
    auto it = empty.begin();
    while (it != empty.end()) {
        std::cout << *it << std::endl;
        ++it;
    }

    return 0;
}
