/*  perfect forwarding
    ~ the following properties of the forwarded arguments are preserved:
      - modifable
      - unmodifable
      - moveable
    
    std::forward<>(arg) casts to rvalue reference but only if arg is NOT lvalue reference
*/

#include <iostream>
#include <iterator>
#include <type_traits>

class X {};

// accepts modifable, unmodifable or moveable
template <typename T>
void g(T&& arg) {}

// accepts modifable, unmodifable or moveable
template <typename T>
void f(T&& arg) { 
    g(std::forward<T>(arg));    // perfectly forwarded (effectively static_cast<T&&>)
                                // if passed as rvalue, arg is lvalue within but cast to rvalue again       OK
                                // if passed as lvalue, arg is lvalue within and nothing is done            OK
                                // instance of arg may have been moved (if cast to rvalue)                  NOTE
}

int main() { 
    X x;
    f(x);                       // f(int&) calls g(int&) as std::forward has no effect

    const X xx;
    f(xx);                      // f(const int&) calls g(const int&) as std::forward has no effect

    f(X());                     // f(int&&) calls g(int&&) as std::forward casts to rvalue
    f(std::move(x));            // f(int&&) calls g(int&&) as std::forward casts to rvalue
                                // instance x no longer exists

    return 0;
}
