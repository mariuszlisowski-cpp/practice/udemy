# script mode: cmake -P <filename>
cmake_minimum_required(VERSION 3.0.0)

set(NAME Alice)
set(Alice Bob)

message(NAME " " ${NAME} " " ${${NAME}})                # NAME is a string
                                                        # ${NAME} is dereferencing a NAME variable
                                                        # ${${NAME}} is dereferencing a NAME variable twice

message(NAME ${NAME} ${${NAME}})                        # no spaces (dereferencing)
message(NAME${NAME}${${NAME}})                          # no spaces (dereferencing)
message("NAME ${NAME} ${${NAME}}")                      # spaces (dereferencing)

set(NAMEAliceBob Charlie)
message(${NAME${NAME}${${NAME}}})                       # output (NAMEAliceBob) dereferenced (no spaces allowed)
