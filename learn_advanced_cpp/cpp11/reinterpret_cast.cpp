#include <iostream>

class Parent {
public:
    virtual void speak() {                                      // make a class plimorphic by adding virtual
        std::cout << "speaking" << std::endl;
    }
};

class Brother : public Parent {
};

class Sister : public Parent {
};

int main() {
    Parent parent;
    Brother brother;
    Sister sister;

    Parent* pParent{ &brother };                                // sub-class object
    Sister* pBrotherA{ static_cast<Sister*>(pParent) };         // UNSAFE
    if (pBrotherA == nullptr) {
        std::cout << "> invalid static cast" << std::endl;
    }
    
    Sister* pBrotherB{ dynamic_cast<Sister*>(pParent) };        // SAFE (here: nullptr)
    if (pBrotherB == nullptr) {
        std::cout << "> invalid dynamic cast" << std::endl;
    }

    Sister* pBrotherC{ reinterpret_cast<Sister*>(pParent) };    // UNSAFE but done here
    if (pBrotherC == nullptr) {
        std::cout << "> invalid reinterpret cast" << std::endl;
    }

    return 0;
}
