#include <exception>
#include <iostream>
#include <new>

void goes_wrong() {
    bool error_a_detected = true;
    bool error_b_detected = false;

    if (error_a_detected) {
        throw std::bad_alloc();
    }
    if (error_b_detected) {
        throw std::exception();
    }
    
}

int main() {
    try {
        goes_wrong();    
    } catch (std::exception& e) {
        std::cout << e.what() << std::endl;             // bad_alloc exceptin caught here
                                                        // as std::exception is a parent of std::bad_exception
    } catch (std::bad_exception& e) {
        std::cout << e.what() << std::endl;             // should be caught here
    }

    return 0;
}
