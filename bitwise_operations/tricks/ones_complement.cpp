#include <bitset>
#include <iostream>
#include <limits>

unsigned long onesComplement_limits(unsigned value) {
    constexpr auto num_of_bits = std::numeric_limits<decltype(value)>::digits;    // without sign bit (!)
                                                                                            // but not a case here
    std::bitset<num_of_bits> mask;
    mask.set();                                                                             // all bits set to 1
                                                                                            // as mask of all ones
    return value ^= mask.to_ulong();
    
}

/* same as above */
unsigned long onesComplement_long(unsigned value) {
    auto all_zeros = value & 0;
    auto all_ones = ~all_zeros;
    
    return value ^= all_ones;
}

/* same as above */
unsigned long onesComplement_short(unsigned value) {
    return value ^= ~(value & 0);                                                           // value & 0 -> all zeros
                                                                                            // then invert to all ones
}

int main() {
    auto word = 0xffff;                                                                 // 16 bits set
    std::cout << std::bitset<32>(word) << std::endl;
    std::cout << std::bitset<32>(onesComplement_short(word))
              << std::endl << std::endl;

    return 0;
}
