#include <exception>
#include <iostream>

class Divisible {
public:
    Divisible(int divisor) {
        if (divisor != 0) {
            divisor_ = divisor;
        } else {
            throw std::invalid_argument("Cannot be zero!");
        }
    }

    bool operator()(int value) {
        return !(value % divisor_);
    } 

private:
    int divisor_{1};
};

int main() {
    Divisible divisible_by_three{3};
    Divisible divisible_by_five{5};

    for (int i = 1; i < 15; ++i) {
        std::cout << (divisible_by_three(i) ? "> by three: " + std::to_string(i).append("\n") : "");
        std::cout << (divisible_by_five(i) ? "> by five: " + std::to_string(i).append("\n") : "");
    }

    try {
        Divisible divisible_by_zero(0);
    } catch (const std::invalid_argument& ex) {
        std::cout << ex.what() << std::endl;
    }

    return 0;
}
