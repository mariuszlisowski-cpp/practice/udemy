#include <memory>
#include <iostream>

struct Foo {
    Foo(int length, double radius)
        : length_(length), radius_(radius) {}

    int length_;
    double radius_;
};

struct Bar {};

int main() {
    std::unique_ptr<Foo> u_ptr{ std::make_unique<Foo>(7, 2.89) };       // pass arguments to constructor

    std::cout << u_ptr->length_ << std::endl;
    std::cout << u_ptr->radius_ << std::endl;
    
    return 0;
}
