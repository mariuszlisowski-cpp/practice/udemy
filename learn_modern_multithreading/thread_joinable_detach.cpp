#include <chrono>
#include <iostream>
#include <thread>

int main() {
    std::thread th1([]{
        std::this_thread::sleep_for(std::chrono::seconds(1));
    });
    std::thread th2([]{
        std::this_thread::sleep_for(std::chrono::seconds(9));
    });

    th2.detach();                                      // not joinable any more

    if (th1.joinable()) {
        th1.join();                                    // can be joined only once
        std::cout << "> thread 1 joined" << std::endl;
    }    

    if (th2.joinable()) {
        th2.join();
        std::cout << "> thread 2 joined" << std::endl;
    }    

    return 0;
}
